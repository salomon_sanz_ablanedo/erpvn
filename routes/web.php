<?php

use Illuminate\Support\Facades\Route;
use App\Models\WsArticle;
use App\Services\StockService;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('importexcel', function(\App\Models\WsArticle $WsArticle){        
    \Illuminate\Support\Facades\Artisan::call('importindexcatalog');
}); */

Route::get('stockfix', function(\App\Models\WsArticle $WsArticle){  
    
    Auth::loginUsingId(2);
    $arts = WsArticle::where('stock','<',0)->get();

    foreach ($arts as $art)    
        StockService::updateStock($art->id, 0);    
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

Route::get('/printorder/{id}', function ($id) 
{
    $order = \App\Models\WsOrder::find($id);

    $data = [
        'order' => $order
    ];
    
    return view('reports.order', $data);
})->name('print.order');

Route::get('/printdeliverynote/{id}', function ($id) 
{
    $note = \App\Models\DeliveryNote::with(['lines.article','order'])->find($id);

    $data = [
        'note' => $note
    ];
    
    return view('reports.deliverynote', $data);
})->name('print.delivery_note');

Route::get('/', function () {
    return view('welcome');
});

