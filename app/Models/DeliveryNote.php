<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryNote extends Model
{
    protected
        $id,
        $date,
        $reference,
        $order_id,
        $customer_id,
        $status,
        $user_id,
        $shipping_costs,
        $show_signature
    ;

    protected $fillable = [
        'date',
        'order_id',
        'reference',
        'status',
        'user_id',
        'shipping_costs',
        'customer_id',
        'show_signature'
    ];

    protected $casts = [
        'date' => 'date'        
    ];

    public function order(){
        return $this->belongsTo(\App\Models\WsOrder::class, 'order_id');
    }

    public function customer(){
        return $this->belongsTo(\App\Models\WsCustomer::class, 'customer_id');
    }

    public function lines(){
        return $this->hasMany(\App\Models\DeliveryNoteLine::class, 'delivery_id');
    }

    public function scopeOfOrder($query, $order_id){
        return $query->where('order_id', $order_id);
    }

    public function user(){
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
