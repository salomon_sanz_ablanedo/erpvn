<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WsStockMovement extends Model
{
    protected 
        $id,
        $article_id,
        $type,
        $quantity,
        $date_movement,
        $user_id;

    protected $casts = [        
        'date_movement' => 'datetime'
    ];

    protected $fillable = [
        'article_id','type','quantity','user_id'
    ];

    public function article(){
        return $this->belongsTo(\App\Models\WsArticle::class, 'article_id');
    }

    public function user(){
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function scopeOfArticle($query, $article_id){
        return $query->where('article_id', $article_id);
    }
}
