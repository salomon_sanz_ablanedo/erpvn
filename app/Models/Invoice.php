<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected 
        $id,
        $date,
        $customer_id
        /* $name,
        $reference,
        $customer_id,
        $stock,
        $stock_min,
        $price */;

   /*  protected $fillable = [
        'stock'
    ]; */    

    public function customer(){
        return $this->belongsTo(\App\Models\WsCustomer::class);
    }
}
