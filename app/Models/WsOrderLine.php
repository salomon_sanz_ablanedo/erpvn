<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WsOrderLine extends Model
{

    protected 
        $id,
        $order_id,
        $article_id,
        $order_ref,
        $obs,
        $quantity,
        $quantity_orig,
        $quantity_supplied,
        $prioritary,
        $status,
        $cancelled_by,
        $cancelled_reason,
        $cancelled_at;

    protected $fillable = ['quantity_supplied'];

    protected $casts = [        
    ];

    public function article(){
        return $this->belongsTo(\App\Models\WsArticle::class, 'article_id');
    }

    public function order(){
        return $this->belongsTo(\App\Models\WsOrder::class, 'order_id');
    }

    public function cancelledBy(){
        return $this->belongsTo(\App\Models\User::class, 'cancelled_by');
    }
}
