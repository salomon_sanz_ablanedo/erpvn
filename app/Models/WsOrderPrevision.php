<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WsOrderPrevision extends Model
{

    protected 
        $id,
        $customer_id,
        $article_id,        
        $quantity,        
        $date_ship,
        $user_id;

    protected $casts = [
        'date_ship' => 'date'        
    ];

    public function customer(){
        return $this->belongsTo(\App\Models\WsCustomer::class);
    }

    public function article(){
        return $this->belongsTo(\App\Models\WsArticle::class, 'article_id');
    }

    public function user(){
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
