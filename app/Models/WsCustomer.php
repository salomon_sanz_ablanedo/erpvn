<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WsCustomer extends Model
{
    protected 
        $id,
        $name,
        $info,
        $address,
        $order_prefix,
        $order_count,
        $deliverynote_count,
        $order_importer_id,
        $order_importer_params,
        $importable_catalog,
        $show_delivery_note_positions,
        $date_last_import_found,
        $date_last_import_success,
        $date_last_import_error,
        $date_last_applicable_order_date;

    protected $casts = [
        'importable_catalog' => 'boolean',                
        'date_last_import_found' => 'datetime',
        'date_last_import_success' => 'datetime',
        'date_last_import_error' => 'datetime',
        'date_last_applicable_order_date' => 'date',
    ];

    public function orders(){
        return $this->hasMany(\App\Models\WsOrder::class, 'customer_id');
    }

    public function orderImporter(){
        return $this->belongsTo(\App\Models\WsOrderImporter::class, 'order_importer_id');
    }
}
