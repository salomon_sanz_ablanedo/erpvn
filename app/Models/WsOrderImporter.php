<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WsOrderImporter extends Model
{

    protected 
        $id,
        $name,        
        $description,
        $job_class;    
}
