<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\WsStockMovement;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Image\Manipulations;
//use Spatie\MediaLibrary\MediaCollections\Models\Media;

class WsArticle extends Model implements HasMedia
{
    use InteractsWithMedia;

    const MAIN_IMG_COLLECTION = 'item_main_img';
    const MAIN_VIDEO_COLLECTION = 'item_main_video';

    public const TYPE_ARTICLE = 'ARTICULO';
    public const TYPE_COMPONENT = 'COMPONENTE';

    protected 
        $id,
        $name,
        $reference,
        $position,
        $image_path, // delete_me after migration
        $customer_id,
        $stock,
        $stock_min,
        $price,
        $seconds_per_unity,
        $quantity_by_box,
        $round_orders_to_box,
        $box_by_pale,
        $type,
        $active;

    protected $fillable = [
        'stock','name','type','reference','position','image_path','customer_id','price','seconds_per_unity','box_by_pale','quantity_by_box','round_orders_to_box','active'
    ];

    public function customer(){
        return $this->belongsTo(\App\Models\WsCustomer::class, 'customer_id');
    }

    public function items(){
        return $this->belongsToMany(\App\Models\WsArticle::class, 'ws_article_items', 'article_id','article_item_id')
                ->withPivot('quantity');
    }

    public function parentItems(){
        return $this->belongsToMany(\App\Models\WsArticle::class, 'ws_article_items', 'article_item_id','article_id')
                ->withPivot('quantity');
    }

    public function movements(){
        return $this->hasMany(\App\Models\WsStockMovement::class, 'article_id');
    }

    public function video(){
        return $this->morphOne(Media::class, 'model')->where('collection_name', '=', static::MAIN_VIDEO_COLLECTION);
    }

    public function isTypeArticle(){
        // do not use this->type (unknow bug, maybe conflict with reserved "type" word)
        return $this->attributes['type'] == static::TYPE_ARTICLE;
    }

    public function isTypeComponent(){
        return $this->attributes['type'] == static::TYPE_COMPONENT;
    }

    public function registerMediaCollections(): void {
        $this->addMediaCollection(static::MAIN_IMG_COLLECTION)->singleFile();
        $this->addMediaCollection(static::MAIN_VIDEO_COLLECTION)->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(130)
            ->height(130)
            ->nonQueued()
            ->performOnCollections(static::MAIN_IMG_COLLECTION)
            ->extractVideoFrameAtSecond(1);

        $this->addMediaConversion('thumbVideo')
            ->fit(Manipulations::FIT_CROP, 125, 125)
            ->nonQueued()
            ->extractVideoFrameAtSecond(20)
            ->performOnCollections(static::MAIN_VIDEO_COLLECTION);
    }

    protected static function booted(): void
    {
        static::addGlobalScope('active', static function (Builder $builder): void {
            $builder->where('active', '=', 1);
        });
    }
}
