<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryNoteLine extends Model
{
    protected 
        $id,
        $delivery_id,        
        $article_id,
        $quantity,
        $order_ref,
        $prioritary,
        $obs,
        $pallet_number;

    protected $fillable = [
        'delivery_id',
        'article_id',
        'quantity',
        'obs',
        'prioritary',
        'order_ref',
        'pallet_number'
    ];

    public function article(){
        return $this->belongsTo(\App\Models\WsArticle::class, 'article_id');
    }

    public function delivery(){
        return $this->belongsTo(\App\Models\DeliveryNote::class, 'delivery_id');
    }
}
