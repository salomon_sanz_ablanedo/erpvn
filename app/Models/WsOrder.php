<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/* use App\Models\Media; */
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
//use Spatie\MediaLibrary\MediaCollections\Models\Media;

class WsOrder extends Model implements HasMedia
{
    use InteractsWithMedia;

    const ORDERFILES_COLLECTION = 'orderfiles';

    protected 
        $id,
        $reference,
        $ext_reference,
        $customer_id,
        $date,
        $observations,
        $creator_id,
        $date_completed
    ;        

    protected $casts = [
        'date' => 'date',
        'date_completed' => 'date'
    ];

    protected $fillable = [
        'reference',
        'ext_reference',
        'customer_id',
        'date',
        'observations',
        'creator_id',
        'date_completed'
    ];

    public function customer(){
        return $this->belongsTo(\App\Models\WsCustomer::class);
    }

    public function creator(){
        return $this->belongsTo(\App\Models\User::class);
    }

    public function lines(){
        return $this->hasMany(\App\Models\WsOrderLine::class, 'order_id');
    }

    public function deliveryNotes(){
        return $this->hasMany(\App\Models\DeliveryNote::class, 'order_id');                
    }

    public function registerMediaCollections(): void {        
        $this->addMediaCollection(static::ORDERFILES_COLLECTION);
    }
}
