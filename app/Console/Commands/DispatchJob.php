<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;


class DispatchJob extends Command
{
    protected $signature = 'job:dispatch {job} 
    {--queue}
    {--p|param=* : The extra parameters to override}
    ';

    protected $description = 'Dispatch job';

    // php artisan job:dispatch OrderImport --param="emailFrom=asdfasdfas@asdfasdf.com"
    // php artisan job:dispatch OrderImport --param="emailFrom=asdfasdfas@asdfasdf.com"
    // php artisan job:dispatch OrderImport --param="customerId=2"

    public function handle()
    {        
        $class = '\\App\\Jobs\\' . $this->argument('job');  
        
        $params = [];
        
        foreach($this->options()['param'] as $value)
        {
            [$key, $val] = explode('=',$value);
            $params[$key] = $val;
        }
        
        if (!empty($this->option('queue')))
        {
            //$this->info('Job queued to: "'.$job->queue.'"');
            dispatch(new $class($params));//->onQueue($job->queue);//$job->queue);
        }
        else
        {                
            dispatch_now(new $class($params));            
        }
        return Command::SUCCESS;
    }
}
