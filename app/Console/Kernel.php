<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\OrderImport;
use Laravel\Nova\Notifications\NovaNotification;
use App\Models\User;
use Illuminate\Support\Stringable;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        //$schedule->job(new Heartbeat)->everyFiveMinutes();
        //$schedule->job(new KybOrderImporter('salomonsanz@gmail.com','1'))
        $schedule->job(new OrderImport)
            ->everyThirtyMinutes()            
            ->onSuccess(function(){               
            })
            ->onFailure(function (Stringable $output) {
                //\Log::info($output);
                foreach(User::all() as $user)
                {                
                    $user->notify(NovaNotification::make()
                        ->message('Ha ocurrido un error al importar un pedido desde el email, contacta con informática.')
                        //->action('Download', URL::remote('https://example.com/report.pdf'))
                        ->icon('exclamation')
                        ->type('error')
                    );
                }
            });
        //$schedule->job(new KybOrderImporter('salomonsanz@gmail.com','2'))->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
