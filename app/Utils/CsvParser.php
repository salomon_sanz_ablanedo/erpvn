<?php

namespace App\Utils;

use Illuminate\Support\Str;

class CsvParser
{
    public static function getCSVParsed($file, $delimiter)
    {
        $csv = array_map(function($line) use ($delimiter){
            return str_getcsv($line, $delimiter);
        }, file($file, FILE_SKIP_EMPTY_LINES));

        $keys = array_map(function($k){
            return Str::slug($k,'_');
        }, array_shift($csv));            

        foreach ($csv as $i=>$row)
            $csv[$i] = array_combine($keys, $row);
        
        return $csv;
    }
}