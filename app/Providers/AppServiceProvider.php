<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\WsStockMovement;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use App\Models\DeliveryNote;
use App\Models\DeliveryNoteLine;
use App\Observers\DeliveryNoteObserver;
use App\Observers\StockObserver;
use App\Observers\OrderObserver;
use App\Observers\OrderLineObserver;
use App\Observers\DeliveryNoteLineObserver;
//use Illuminate\Support\Facades\Config;
//use Illuminate\Support\Carbon;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        WsStockMovement::observe(StockObserver::class);
        WsOrder::observe(OrderObserver::class);
        WsOrderLine::observe(OrderLineObserver::class);
        DeliveryNoteLine::observe(DeliveryNoteLineObserver::class);
        DeliveryNote::observe(DeliveryNoteObserver::class);                
    }
}
