<?php

namespace App\Observers;

use App\Models\WsOrder;
use Auth;
use Inani\LaravelNovaConfiguration\Helpers\Configuration;

class OrderObserver
{
    public function creating(WsOrder $wsOrder)
    {
        $customer = $wsOrder->customer;
        $count = $customer->order_count + 1;        

        $wsOrder->reference = ($wsOrder->customer->order_prefix ?? '') . str_pad($count, 5, '0', STR_PAD_LEFT);

        if (empty($wsOrder->creator_id) && Auth::check()) 
            $wsOrder->creator_id = Auth::user()->id;
        
        $customer->order_count = $count;
        $customer->save();
    }

    public function created(WsOrder $WsOrder){
        
    }

    public function updated(WsOrder $WsOrder){
        
    }

    public function deleted(WsOrder $WsOrder){
        
    }

    public function restored(WsOrder $WsOrder){
        //
    }

    public function forceDeleted(WsOrder $WsOrder){
        //
    }
}
