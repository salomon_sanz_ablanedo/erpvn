<?php

namespace App\Observers;

use App\Models\DeliveryNote;
use Auth;
use App\Services\OrderService;
use App\Services\StockService;

class DeliveryNoteObserver
{
    public $afterCommit = true;

    public function creating(DeliveryNote $note)
    {
        if (empty($wsOrder->user_id) && Auth::check()) 
            $note->user_id = Auth::user()->id;

        $customer = $note->customer;

        if (empty($customer->deliverynote_count))        
            $reference = 'ALB-' . str_pad($note->id, 6, '0', STR_PAD_LEFT);                                
        else
        {
            $count = $customer->deliverynote_count + 1;        
            $reference = 'ALB-' . $note->customer->order_prefix . str_pad($count, 6, '0', STR_PAD_LEFT);
            $customer->deliverynote_count = $count;
            $customer->save();
        }

        $note->reference = $reference;
        $note->save();
    }

    public function created(DeliveryNote $note){
        //$this->updateDeliveryUnits($note);  
    }

    public function updated(DeliveryNote $note){
        //$this->updateDeliveryUnits($note);
    }

    public function deleted(DeliveryNote $note){

        $note->lines->each(function($line){
            StockService::addMovement($line->article_id, $line->quantity);
        });

        $this->updateDeliveryUnits($note);
    }

    public function restored(DeliveryNote $note){
        $this->updateDeliveryUnits($note);
    }

    public function forceDeleted(DeliveryNote $note){
        $this->updateDeliveryUnits($note);
    }

    private function updateDeliveryUnits($note){
        OrderService::instance()->updateDeliveredUnits($note->order);
    }
}
