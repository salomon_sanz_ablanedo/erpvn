<?php

namespace App\Observers;

use App\Models\DeliveryNoteLine;
use Auth;
use Inani\LaravelNovaConfiguration\Helpers\Configuration;
use App\Services\OrderService;
use App\Services\StockService;
use Log;

use Exception;

class DeliveryNoteLineObserver
{
    public $afterCommit = false;
    
    public function creating(DeliveryNoteLine $line)
    {        
        $this->checkArticleBelongsToOrder($line);
        $this->checkQuantityToAddIsValid($line);
    }

    public function created(DeliveryNoteLine $line){
/*         $this->checkArticleBelongsToOrder($line);
        $this->checkQuantityToAddIsValid($line);      */         
        $this->updateDeliveryUnits($line);  
        $this->handleStockDiffFromLine($line);
    }

    public function updated(DeliveryNoteLine $line){
        $this->checkArticleBelongsToOrder($line);        
        $this->checkQuantityToUpdateIsValid($line);
        $this->updateDeliveryUnits($line);
        $this->handleStockDiffFromLine($line);
    }

    public function deleted(DeliveryNoteLine $line){
        $this->updateDeliveryUnits($line);
        StockService::addMovement($line->article_id, $line->quantity);        
    }

    public function restored(DeliveryNoteLine $line){
        $this->updateDeliveryUnits($line);
        $this->handleStockDiffFromLine($line);
    }

    public function forceDeleted(DeliveryNoteLine $line){
        $this->updateDeliveryUnits($line);
        $this->handleStockDiffFromLine($line);
    }

    private function updateDeliveryUnits($line){
        OrderService::instance()->updateDeliveredUnits($line->delivery->order);
    }

    private function handleStockDiffFromLine($line){
        $diff = ($line->getOriginal('quantity') ?? 0) - $line->quantity;        
        StockService::addMovement($line->article_id, $diff);
    }

    private function checkArticleBelongsToOrder($delivery_line)
    {       
        throw_if(
            empty($this->getOrderLine($delivery_line)),
            Exception::class,
            'El artículo #'.$delivery_line->article_id.' ('.$delivery_line->article->reference.') no es válido, no pertenece a ningún elemento del pedido'
        );
    }

    private function checkQuantityToAddIsValid($delivery_line)
    {
        $order_line = $this->getOrderLine($delivery_line);

        throw_if(
            ($order_line->quantity_supplied + $delivery_line->quantity) > $order_line->quantity,
            Exception::class,
            'La cantidad introducida no es válida, es superior a la cantidad del pedido. La cantidad máxima válida es: '.($order_line->quantity - $order_line->quantity_supplied)
        );        
    }

    private function checkQuantityToUpdateIsValid($delivery_line)
    {
        $diff = $delivery_line->quantity - ($delivery_line->getOriginal('quantity') ?? 0);        

        if ($diff <= 0) return;

        $order_line = $this->getOrderLine($delivery_line);

        throw_if(
            ($order_line->quantity_supplied + $diff) > $order_line->quantity,
            Exception::class,
            'La cantidad introducida a actualizar no es válida.'
        );        
    }

    private function getOrderLine($delivery_line)
    {
        return $delivery_line->delivery->order->lines->first(function ($order_line) use($delivery_line){
            return $order_line->article_id == $delivery_line->article_id;
        });
    }
}
