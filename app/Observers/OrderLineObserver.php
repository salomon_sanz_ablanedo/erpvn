<?php

namespace App\Observers;

use App\Models\WsOrderLine;
use App\Services\OrderService;
use Auth;
use Inani\LaravelNovaConfiguration\Helpers\Configuration;

class OrderLineObserver
{
    public function creating(WsOrderLine $line)
    {
        
    }

    public function created(WsOrderLine $line){

    }

    public function updated(WsOrderLine $line)
    {
        
    }

    public function deleted(WsOrderLine $line){
        
    }

    public function restored(WsOrderLine $line){
        //
    }

    public function forceDeleted(WsOrderLine $line){
        //
    }
}
