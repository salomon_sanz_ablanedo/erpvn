<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Http\Requests\NovaRequest;
use Storage;

class WsStockMovement extends Resource
{
    public static $displayInNavigation = true;

    public static $model = \App\Models\WsStockMovement::class;

    public static $title = 'id';

    public static $group = 'Taller';

    public static $search = [];

    public static $with = ['article.customer','user'];

    public static $globallySearchable = false;

    public static function label(){
        return 'Movs. de almacén';
    }

    public static function labelSingular(){
        return 'Mov. de almacén';
    }

    public function fields(Request $request)
    {
        return [      
            BelongsTo::make('Artículo / Componente', 'article', \App\Nova\WsArticle::class)
                ->searchable()
                ->withSubtitles()
                ->rules('required')
                ->filterable(),

            Text::make('Cliente', function(){
                return $this->article->customer->name;
            })->readonly()
            ->filterable(),    

            Badge::make('Tipo', function(){
                return $this->article->type;
            })
            ->map([
                'COMPONENTE' => 'warning',
                'ARTICULO' => 'info'
            ])
            ->showOnDetail()
            ->showOnIndex()
            ->sortable()
            ->filterable()
            ->readonly(),     

            Number::make('Cantidad','quantity')
                ->rules('required')                
                ->step(1)
                ->help('Si es retirada de stock, poner cantidad negativa')
                ->displayUsing(function($value){
                    if ($value > 0)
                        return '<span style="color:green;">+'.$value.'</span>';
                    else
                        return '<span style="color:red;">'.$value.'</span>';
                })->asHtml(),

            DateTime::make('Fecha', 'date_movement')                
                ->readonly()
                ->exceptOnForms()
                ->filterable()
                ->sortable(),
            /* Text::make('Fecha', function(){
                return date('d-m-y', strtotime($this->date_movement));
            }), */
            

            BelongsTo::make('Usuario', 'user', \App\Nova\User::class)
                ->hideWhenCreating()
                ->hideWhenUpdating(),
            /* Avatar::make('Usuario')->thumbnail(function(){
                return $this->user?->getFirstMediaUrl('avatar', 'thumb');                
            })->hideWhenCreating()
              ->hideWhenUpdating(), */

            // DateTime::make('Fecha mvto.', 'date_movement')
            //     ->rules('required')
            //     ->firstDayOfWeek(1)
            //     ->default(function ($request) {
            //         return date("Y-m-d H:i:s");
            //     })
        ];
    }

    public function cards(Request $request){
        return [];
    }

    public function filters(Request $request){
        return [
            new \App\Nova\Filters\StockMovementCustomerFilter()
        ];
    }

    public function lenses(Request $request){
        return [];
    }

    public function actions(Request $request){
        return [];
    }
}
