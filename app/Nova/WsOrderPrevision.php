<?php

namespace App\Nova;

use Illuminate\Http\Request;
use DB;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Http\Requests\NovaRequest;
use Carbon\Carbon;
use App\Nova\Filters\OrderLinesAndPrevisionsObsFilter;
use Coroowicaksono\ChartJsIntegration\StackedChart;
use Illuminate\Support\Facades\Log;
use App\Services\ArticleService;


class WsOrderPrevision extends Resource
{
    
    public static $model = \App\Models\WsOrderPrevision::class;

    public static $title = 'id';

    public static $search = ['article.name', 'article.reference'];
    public static $searchable = true;
    
    public static $group = 'Taller';
    public static $tableStyle = 'tight';
    
    public static $with = ['user'];

    public static function label(){
        return __('Previsiones');
    }

    public static function singularLabel(){
        return __('Previsión');
    }

    public function fields(NovaRequest $request)
    {
        return [     

            ID::make()->sortable(),
            BelongsTo::make('Cliente', 'customer', \App\Nova\WsCustomer::class)
                ->rules('required')
                ->filterable(),

            BelongsTo::make('Artículo', 'article', \App\Nova\WsArticle::class)->rules('required')
                ->searchable(),

            Number::make('Unidades', 'quantity')->min(1)->step(1)->rules('required'),
            Date::make('Fecha entrega', 'date_ship')
                //->min(Carbon::today()->addDay(1))
                ->rules('required')
                ->sortable()
                ->filterable(),

            BelongsTo::make('Usuario', 'user', \App\Nova\User::class)
                ->hideWhenCreating()
                ->hideWhenUpdating(),
            /* Avatar::make('Usuario')->thumbnail(function(){
                return $this->user?->getFirstMediaUrl('avatar', 'thumb');                
            })->hideWhenCreating()
              ->hideWhenUpdating(), */
                
            BelongsTo::make('Usuario', 'user', \App\Nova\User::class)
                ->filled(function(NovaRequest $request, $model){
                    $model->user_id = \Auth::user()->id;
                })
                ->nullable()
                ->showOnCreating()
                ->showOnUpdating()
                ->hideFromDetail()
                ->hideFromIndex(),    
                
            Text::make('Tiempo requerido', function(){
                return ArticleService::getOrderTime4HumansByArticle($this->article, $this->quantity, false);
            })->readonly()                
        ];
    }

    public function getWorkByDayByCustomerData()
    {        
        $from = Carbon::now()->format('Y-m-d');
        $to = Carbon::now()->add(20, 'day')->format('Y-m-d');

        $query_previsions = 'SELECT ROUND(SUM(a.seconds_per_unity * p.quantity / 60 /60), 1) as total, DATE_FORMAT(p.date_ship, \'%d/%m\') as dateship, c.name as customer_name 
            FROM `ws_order_previsions` p 
            INNER JOIN ws_articles a ON p.article_id=a.id 
            INNER JOIN ws_customers c ON p.customer_id=c.id 
            WHERE p.date_ship >= "'.$from.'" 
            AND p.date_ship <="'.$to.'" 
            GROUP BY date_ship, customer_name 
            ORDER BY date_ship ASC
        ';

        /* $query_orders = 'SELECT ROUND(SUM(a.seconds_per_unity * l.quantity / 60 /60), 1) as total, DATE_FORMAT(o.date, \'%d/%m\') as dateship, c.name as customer_name 
            FROM `ws_orders` o 
            INNER JOIN ws_order_lines l ON l.order_id=o.id 
            INNER JOIN ws_articles a ON l.article_id=a.id 
            INNER JOIN ws_customers c ON o.customer_id=c.id 
            WHERE o.date >= "'.$from.'" 
            AND o.date <="'.$to.'" 
            AND o.status="PENDING" 
            AND l.quantity != l.quantity_supplied
            GROUP BY date, customer_name 
            ORDER BY date ASC
        '; */

        /* $query_orders_acumulated = 'SELECT ROUND(SUM(a.seconds_per_unity * l.quantity / 60 /60), 1) as total, c.name as customer_name 
            FROM `ws_orders` o 
            INNER JOIN ws_order_lines l ON l.order_id=o.id 
            INNER JOIN ws_articles a ON l.article_id=a.id 
            INNER JOIN ws_customers c ON o.customer_id=c.id 
            WHERE o.date < "'.$from.'" 
            AND o.status="PENDING" 
            AND l.quantity != l.quantity_supplied
            GROUP BY customer_name
        '; */

        $result_previsions = DB::select($query_previsions);
        /* $result_orders = DB::select($query_orders);
        $result_orders_accumulated = DB::select($query_orders_acumulated); */

        /* foreach($result_orders_accumulated as &$acc)
            $acc->dateship = 'acumulado'; */

        //\Log::info($result_orders_accumulated);

        $result = array_merge($result_previsions);

        $distinct_days = [];
        $distinct_customers = []; 
        $series = [];       
        $colors = [
            "#ffcc5c","#91e8e1","#ff6f69","#b088d8","#d8b088", "#88b0d8", "#6f69ff"
        ];

        foreach($result as $row)
        {
            if (!in_array($row->customer_name, $distinct_customers))
                $distinct_customers[] = $row->customer_name;

            if (!isset($distinct_days[$row->dateship]))            
                $distinct_days[$row->dateship] = [];            

             if (!isset($distinct_days[$row->dateship][$row->customer_name]))
                $distinct_days[$row->dateship][$row->customer_name] = $row->total;
        }

        foreach($distinct_days as $date => &$day)
        {
            foreach($distinct_customers as $customer)
            {
                if (!array_key_exists($customer, $day))
                    $day[$customer]=0;
            }            
        }

        foreach($distinct_customers as $index => $customer)
        {
            $serie = [
                'barPercentage' => 0.5,
                'label' => $customer,
                'backgroundColor' => $colors[$index],
                'data'  => []
            ];

            foreach($distinct_days as $d)
                $serie['data'][] = $d[$customer];            

            $series[] = $serie;
        }

        return [
            'series' => $series,
            'categories' => array_keys($distinct_days)
        ];
    }

    public function cards(NovaRequest $request)
    {
        //return [];
        
        //$workByDayByCustomerData = $this->getWorkByDayByCustomerData();

        return [
            /*
            (new StackedChart())
                ->title('Trabajo pendiente en horas (pedidos + previsiones)')
                ->series($workByDayByCustomerData['series'])
                ->options([
                    'showTotal' => false,
                    'showPercentage' => true,
                    'xaxis' => [
                        'categories' => $workByDayByCustomerData['categories']    
                    ],
                    // 'btnRefresh' => true         ,
                    //'btnReload' => true            
                ])
                ->withMeta([
                    'id'=>'asdf123213123'
                ])
                ->width('full'), */ 
                //\App\Nova\Metrics\PrevisionsTotalWorktime::make()
        ];
        
    }

    public function filters(NovaRequest $request)
    {
        return [
            new OrderLinesAndPrevisionsObsFilter,
        ];
    }

    public function lenses(NovaRequest $request)
    {
        return [];
    }

    public function actions(NovaRequest $request)
    {
        return [];
    }
}
