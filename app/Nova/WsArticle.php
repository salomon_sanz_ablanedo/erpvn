<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\HasMany;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Ebess\AdvancedNovaMediaLibrary\Fields\Media;
use Illuminate\Support\Str;
use Carbon\CarbonInterval;

// filters
use App\Nova\Filters\ArticleTypeFilter;
use App\Nova\Filters\ArticleTimeFilter;

// actions
use App\Nova\Actions\DefineArticleStockAction;
use App\Nova\Actions\DeleteArticle;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

use Illuminate\Support\Facades\Storage;

class WsArticle extends Resource
{
    public static $model = \App\Models\WsArticle::class;

    //public static $title = 'name';

    public static $tableStyle = 'tight';

    public  function title(){
        $name = '';

        if (!empty($this->reference))
            $name .= $this->reference . ' | ';

        $name .= $this->name;
        return $name;
    }

    public function subtitle()
    {
        $text = $this->customer ? $this->customer->name : '';

        if (!empty($this->reference))
        {
            $text .= !empty($text) ?  ' | ' : '';
            $text .= 'Ref: '.$this->reference;
            $text .= ' | '. $this->type;
        }

        return $text;
    }

    public static $with = ['customer','items','video'];

    public static $search = [
        'name','reference'
    ];

    public static $group = 'Taller';

    public static function label(){
        return __('Artículos');
    }

    public static function singularLabel(){
        return __('Artículo');
    }

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make('Referencia', 'reference')->sortable(),

            // Avatar::make('image_path')
            //     ->hideFromIndex()
            //     ->hideFromDetail()
            //     ->hideWhenCreating()
            //     ->hideWhenUpdating(),
           /*  Image::make('Imagen','image_path')
                    ->disk('public')
                    ->path('/articles'), */
            Images::make('Imagen', 'item_main_img')
                ->conversionOnIndexView('thumb'),

            Text::make('Nombre', 'name')
                ->sortable()
                ->onlyOnIndex()
                ->displayUsing(function($name){
                    return Str::limit($name, 30);
                }),               

            Text::make('Nombre', 'name')                
                ->rules('required')
                ->hideFromIndex(),

            Select::make('Tipo','type')->options([
                    'COMPONENTE' => 'Componente',
                    'ARTICULO' => 'Artículo'
                ])->onlyOnForms(),
            
            BelongsTo::make('Cliente', 'customer', \App\Nova\WsCustomer::class)
                ->nullable()
                ->sortable()
                ->filterable(),

            Text::make('Stock')
                ->exceptOnForms()
                ->sortable()
                ->displayUsing(function($val){
                    if ($val === null)
                        return '--';
                    else
                    {
                        if (empty($this->stock_min))
                            return $val;
                        else if ($this->stock < $this->stock_min)
                            return '<span style="color:red;">'.$this->stock.' ⚠️</span>';
                        else
                            return $val;
                    }
                })->asHtml(),                

            Number::make('Stock Mínimo', 'stock_min')
                ->min(1)
                ->step(1)
                ->sortable()
                ->filterable()
                ->displayUsing(function($val){
                    if ($val === null)
                        return '--';
                    else
                    {
                        return $val;
                    }
                })->asHtml(),

            Badge::make('Tipo','type')
                ->map([
                    'COMPONENTE' => 'warning',
                    'ARTICULO' => 'info'
                ])
                ->showOnDetail()
                ->showOnIndex()
                ->sortable()
                ->filterable()
                ->readonly(), 

            
            // for detail/index, is incompatible "showFieldConditionally" with dependsOn
            $this->showFieldConditionally(
                Number::make('Tiempo de fabricación', 'seconds_per_unity')
                    ->help('Cuantos segundos cuesta fabricar una unidad')
                    ->nullable()
                    ->exceptOnForms()
                    ->sortable()
                    ->filterable()
                    ->displayUsing(function($value){
                        if (!empty($value))
                        {
                            // this is for decimals
                            if ($value < 60)
                                return str_replace('.',',', $value .' seg.');

                            $result = CarbonInterval::seconds($value)->cascade()->forHumans();
                            return str_replace(['horas','hora','minutos','minuto','segundos','segundo'], ['h.', 'h.', 'min.', 'min.', 'seg.', 'seg.'],$result);
                            /* if ($value >= 3600)                            
                                return $value . ' segs.';
                            elseif ($value >= 60)
                                return gmdate("H:i:s", $value);
                            else
                                return $value . ' segs.'; */
                        }
                        else
                            return '--';                        
                    }),                
                    //->showWhen($this->type !== 'SERVICE'),
                $this->type == 'ARTICULO'
            ),

            // for form is incompatible "showFieldConditionally" with dependsOn            
            Number::make('Tiempo de fabricación', 'seconds_per_unity')
                ->help('Cuantos segundos cuesta fabricar una unidad')
                ->onlyOnForms()
                ->nullable()
                ->sortable()
                ->filterable()
                ->step('any')
                ->displayUsing(function($value){
                    return $value . ' segundos';
                })
                ->placeholder('segundos')
                //->showWhen($this->type !== 'SERVICE')
                ->dependsOn('type', function($field, $request, $formData){
                    if ($formData->type === 'COMPONENTE')
                        $field->hide();
                    else
                        $field->show();
                }),//->hideByDefault(),                         
                
            // for detail/index, is incompatible "showFieldConditionally" with dependsOn            
            $this->showFieldConditionally(
                Number::make('Precio','price')                    
                    ->min(0)
                    ->step('any')
                    ->nullable()
                    ->exceptOnForms()
                    ->sortable()
                    ->filterable()
                    ->displayUsing(function($value){
                        if (!empty($value))
                            return $value .' €';                        
                    }),
                    //->showWhen($this->type == 'ARTICULO')                
                $this->type == 'ARTICULO'
            ),

             // for detail/index, is incompatible "showFieldConditionally" with dependsOn                         
            Number::make('Precio','price')                    
                ->min(0)
                ->step('any')
                ->nullable()
                ->sortable()
                ->filterable()
                ->onlyOnForms()
                ->displayUsing(function($value){
                    if (!empty($value))
                        return $value .' €';                        
                })
                //->showWhen($this->type == 'ARTICULO')
                ->dependsOn('type', function($field, $request, $formData){
                    if ($formData->type === 'COMPONENTE')
                        $field->hide();
                    else
                        $field->show();
                }),

            $this->showFieldConditionally(
                Text::make('€/h')                                        
                    ->displayUsing(function($value){
                        if (empty($this->price) || empty($this->seconds_per_unity))
                            return null;
                        
                        return (string) round(3600 / $this->seconds_per_unity * $this->price, 2) . '€';                        
                    })
                    //->showWhen($this->type == 'ARTICULO')
                    ->dependsOn('type', function($field, $request, $formData){
                        if ($formData->type === 'COMPONENTE')
                            $field->hide();
                        else
                            $field->show();
                    })
                    ->readonly(),
                $this->type == 'ARTICULO'
            ),

            $this->showFieldConditionally(        
                Number::make('Posición', 'position')                
                    ->nullable()
                    ->min(0)
                    ->step(1)
                    ->filterable()
                    ->sortable()
                    ->hideFromIndex(function ($request) {
                        return $request->viaRelationship();
                    }),
                    $this->type == 'ARTICULO'
            ),

            // only for form (dependsOn is not compatible with "showFieldConditionally)            
            Number::make('Unidades por caja', 'quantity_by_box')                
                ->nullable()                
                ->min(1)
                ->step(1)
                ->sortable()
                ->filterable()
                ->dependsOn('type', function($field, $request, $formData){
                    if ($formData->type === 'ARTICULO')
                        $field->show();
                    else
                        $field->hide();
                }),

        // only for form (dependsOn is not compatible with "showFieldConditionally)
            Boolean::make('Redondear pedidos a uds/caja', 'round_orders_to_box')                
                ->help('Algunos proveedores requieren que sean cajas completas. Por ejemplo si llega un pedido de 300 muelles, pero realmente se entregan en cajas de 400 en 400, si esta casilla está marcada y está relleno el dato de "Unidades por caja" el sistema redondeará a ese valor el pedido.')
                ->dependsOn('type', function($field, $request, $formData){
                    if ($formData->type === 'ARTICULO')
                        $field->show();
                    else
                        $field->hide();
                })
                ->sortable()
                ->filterable(),            

            $this->showFieldConditionally(
                Number::make('Cajas por palé', 'box_by_pale')                
                    ->nullable()
                    ->min(0)
                    ->step(1)
                    ->sortable()
                    ->filterable()
                    ->hideFromIndex(function ($request) {
                        return $request->viaRelationship();
                    }),
                    $this->type == 'ARTICULO'
            ),

/*             File::make('Video','video_path')
                    ->disk('public')
                    ->path('/videos')
                    ->onlyOnForms()
                    ->acceptedTypes('video/*'), */

            Media::make('Video', constant(static::$model .'::MAIN_VIDEO_COLLECTION')) // media handles videos
                    ->conversionOnIndexView('thumbVideo')
                    ->conversionOnDetailView('thumbVideo')
                    //->singleMediaRules('max:5000') // max 5000kb
                    ->rules('max:10240') // Tamaño máximo en kilobytes (10 MB)
                    ->hideFromIndex()
                    ->hideFromDetail(),

            Text::make('Instrucciones')
                    ->onlyOnDetail()
                    ->displayUsing(function()
                    {
                        //$item->video360->getUrl('')
                        if (empty($this->video))
                            return '';
                        $html = '<video width="320" height="240" controls>
                                    <source src="'.$this->video->getUrl('').'" type="video/mp4">                                    
                                    Your browser does not support the video tag.
                                </video>';
                                /*
                                '<video width="320" height="240" controls>
                                    <source src="'.$this->video->getUrl('').'" type="video/mp4">
                                    <source src="'.$this->video->getUrl('').'" type="video/movie">
                                    <source src="'.$this->video->getUrl('').'" type="video/avi">
                                    Your browser does not support the video tag.
                                </video>'
                                */
                        return $html;
                    })->asHtml(),            

            Trix::make('Descripcion', 'description')
                ->alwaysShow(),                                     

            // Text::make('Archivo', function() use($width, $height)
            // {
            //     $html = '<video width="320" height="240" controls>
            //                 <source src="'.Storage::disk('public')->url($this->path).'" type="video/mp4">
            //                 Your browser does not support the video tag.
            //             </video>';

            // })
            // ->asHtml(),


            $this->showFieldConditionally(
                BelongsToMany::make('Componentes', 'items', \App\Nova\WsArticle::class)
                    ->searchable()
                    ->fields(function () {
                        return [
                            Number::make('Cantidad', 'quantity')
                                ->rules('required')                                
                                ->min(0)
                                ->step('any')
                                ->displayUsing(function($value){

                                    if (empty($value)) return;

                                    $result = 'x'.$value;
                                    /* if ($result < 1)
                                        $result .= '('.intval(1/$value).' por caja)'; */
                                    return $result;
                                })
                        ];
                    }),
                  $this->type !== 'COMPONENTE' // why doesnt work == "SERVICE" , mistery
            ),

           $this->showFieldConditionally(
                BelongsToMany::make('Es subcomponente de', 'parentItems', \App\Nova\WsArticle::class)
                    ->searchable()
                    ->fields(function() {
                        return [
                            Number::make('Cantidad', 'quantity')
                                ->rules('required')                                
                                ->min(0)
                                ->displayUsing(function($item){
                                    if (!empty($item))
                                        return 'x'.$item;
                                })
                        ];
                    }),
                $this->type !== 'ARTICULO' // why doesnt work == "COMPONENTE" , mistery
            ),

            HasMany::make('Mvtos. de Almacén', 'movements', \App\Nova\WsStockMovement::class)
                // ->fields(function () {
                //     return [
                //         Number::make('Cantidad', 'quantity'),
                //         Date::make('Fecha', 'date_movement')
                //     ];
                // }),
        ];
    }

    protected function showFieldConditionally($field, $show)
    {
        if ($show)
            return $field;
        else
            return $this->merge([]);
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new ArticleTypeFilter,
            new ArticleTimeFilter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new DefineArticleStockAction(),
            \App\Nova\Actions\FixStockMovementArt2Comps::make()->standalone()->sole()->exceptInline()->canRun(function($request, $model){
                return $model->type == 'ARTICULO';
            }),
            (new DownloadExcel())
                ->withFilename('articles-' . time() . '.csv')
                ->withWriterType(\Maatwebsite\Excel\Excel::CSV)
                ->withHeadings()
                ->only('name', 'reference','type','stock','stock_min','price','seconds_per_unity'),
            DeleteArticle::make()->standalone()->sole()->exceptInline()
        ];
    }
}
