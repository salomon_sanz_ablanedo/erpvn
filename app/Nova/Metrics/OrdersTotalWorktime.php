<?php

namespace App\Nova\Metrics;

use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Value;
use Laravel\Nova\Nova;
use DB;

class OrdersTotalWorktime extends Value
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    //public $icon = 'clock';
    //public $icon = false;

    public $name = 'Pedidos (tiempo pte.)';

    public function calculate(NovaRequest $request)
    {
        $result = DB::select('SELECT SUM(a.seconds_per_unity * l.quantity) as total FROM `ws_order_lines` l         
            INNER JOIN ws_articles a ON l.article_id=a.id 
            INNER JOIN ws_orders o ON l.order_id = o.id
            WHERE o.status="PENDING" 
            AND l.quantity != l.quantity_supplied');        

        return $this->result(round($result[0]->total / 60 / 60))->allowZeroResult()
            ->suffix('horas');
            //->previous(50);
            //->prefix('$')
        //return $this->count($request, Model::class);
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            //1 => Nova::__('7 Days'),
            /* 
            60 => Nova::__('60 Days'),
            365 => Nova::__('365 Days'),             
            */
            //'TOMORROW' => Nova::__('Tomorrow'),
            /* 'MTD' => Nova::__('Month To Date'),
            'QTD' => Nova::__('Quarter To Date'),
            'YTD' => Nova::__('Year To Date'), 
            */             
        ];
    }

    /**
     * Determine the amount of time the results of the metric should be cached.
     *
     * @return \DateTimeInterface|\DateInterval|float|int|null
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }
}
