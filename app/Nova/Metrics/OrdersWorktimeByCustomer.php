<?php

namespace App\Nova\Metrics;

use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Partition;

class OrdersWorktimeByCustomer extends Partition
{
    public $name = 'Tiempo total pedidos actuales por cliente';
    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    public function calculate(NovaRequest $request)
    {
        return $this->result([
            'KYBSE' => 100,
            'KAMS' => 200,
            'INAPLAS' => 300,
        ]);
       /*  ->label(fn ($value) => match ($value) {
            null => 'None',
            default => rand(0, 100) . '%'
        }); */
    }

    /**
     * Determine the amount of time the results of the metric should be cached.
     *
     * @return \DateTimeInterface|\DateInterval|float|int|null
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'orders-worktime-by-customer';
    }
}
