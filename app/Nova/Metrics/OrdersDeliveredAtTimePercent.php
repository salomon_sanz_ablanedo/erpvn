<?php

namespace App\Nova\Metrics;

use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Value;
use Laravel\Nova\Nova;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class OrdersDeliveredAtTimePercent extends Value
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    public $icon = 'check';
    //public $icon = false;
    public $range = 7;

    public $name = 'Entrega a tiempo';

    public function calculate(NovaRequest $request)
    {
        //$from = Carbon::now()->sub($request->get('range'), 'day')->format('Y-m-d');
        $range = request('range', 30);

        $from = Carbon::now()->sub($range, 'day')->format('Y-m-d');
        $to = Carbon::now()->format('Y-m-d');

        $q_total = 'SELECT COUNT(*) as total FROM `ws_orders` WHERE date >="'.$from.'" AND date <= "'.$to.'"';
        $q_late  = 'SELECT COUNT(*) as total FROM `ws_orders` WHERE date >="'.$from.'" AND date <= "'.$to.'" AND ((date_completed IS NOT NULL AND DATE(date_completed) > date) OR (date_completed IS NULL AND date < CURDATE()))';        

        $total = DB::select($q_total)[0]->total;
        
        if ($total > 0)
        {
            $total_late = DB::select($q_late)[0]->total;                        
        }
        else
        {
            $total = 100;
            $total_late = 100;
        }

        $total_at_time = $total - $total_late;        

        $percent = round($total_at_time * 100 / $total);

        return $this->result($percent)->allowZeroResult()            
            ->suffix('%');
            //->previous(50);
            //->prefix('$')
        //return $this->count($request, Model::class);
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            7 => Nova::__('7 Days'),            
            15 => Nova::__('15 Days'),
            30 => Nova::__('30 Days'),            
            //'TOMORROW' => Nova::__('Tomorrow'),
            /* 'MTD' => Nova::__('Month To Date'),
            'QTD' => Nova::__('Quarter To Date'),
            'YTD' => Nova::__('Year To Date'), 
            */             
        ];
    }

    /**
     * Determine the amount of time the results of the metric should be cached.
     *
     * @return \DateTimeInterface|\DateInterval|float|int|null
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }
}
