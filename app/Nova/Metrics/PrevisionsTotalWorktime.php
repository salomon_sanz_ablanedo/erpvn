<?php

namespace App\Nova\Metrics;

use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Value;
use Laravel\Nova\Nova;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log; 

class PrevisionsTotalWorktime extends Value
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    //public $icon = 'globe';
    public $icon = false;

    public $name = 'Tiempo de trabajo previsto';

    public function calculate(NovaRequest $request)
    {        
        $from = Carbon::now()->add(1, 'day')->format('Y-m-d');
        $to = Carbon::now()->add($request->get('range'), 'day')->format('Y-m-d');

        $query = 'SELECT SUM(a.seconds_per_unity * p.quantity) as total FROM `ws_order_previsions` p         
        INNER JOIN ws_articles a ON p.article_id=a.id             
        WHERE p.date_ship >= "'.$from.'" AND p.date_ship <="'.$to.'"';        

        $result = DB::select($query);

        return $this->result(round($result[0]->total / 60 / 60))->allowZeroResult()
            ->suffix('horas');
            //->previous(50);
            //->prefix('$')
        //return $this->count($request, Model::class);
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            //1 => Nova::__('7 Days'),
            1 => 'Mañana',
            7 => 'Próx. 7d.',
            /* 
            60 => Nova::__('60 Days'),
            365 => Nova::__('365 Days'),             
            */
            /* 'MTD' => Nova::__('Month To Date'),
            'QTD' => Nova::__('Quarter To Date'),
            'YTD' => Nova::__('Year To Date'), 
            */             
        ];
    }

    /**
     * Determine the amount of time the results of the metric should be cached.
     *
     * @return \DateTimeInterface|\DateInterval|float|int|null
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }
}
