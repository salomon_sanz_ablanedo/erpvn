<?php

namespace App\Nova\Metrics;

use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Trend;
use Laravel\Nova\Nova;
use DB;
use Carbon\Carbon;
use Laravel\Nova\Metrics\TrendResult;
use Illuminate\Support\Arr;

class PrevisionsWorktimeByDays extends Trend
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    //public $icon = 'globe';
    //public $icon = false;

    //public $name = 'Previsión próximos días';

    public function name(){
        return 'Previsión próximos días';
    }

    public function calculate(NovaRequest $request)
    {                    
        $from = Carbon::now()->add(1, 'day')->format('Y-m-d');
        $to = Carbon::now()->add(30/* $request->get('range') */, 'day')->format('Y-m-d');
        //$to = Carbon::now()->add($request->get('range'), 'day')->format('Y-m-d');

        $query = 'SELECT CEIL(SUM(a.seconds_per_unity * p.quantity / 60 /60)) as total, DATE_FORMAT(p.date_ship, \'%a %d/%m\') as dateship FROM `ws_order_previsions` p         
        INNER JOIN ws_articles a ON p.article_id=a.id
        WHERE p.date_ship >= "'.$from.'" AND p.date_ship <="'.$to.'" GROUP BY date_ship ORDER BY date_ship ASC';        

        $result = DB::select($query);

        $result = Arr::pluck($result, 'total', 'dateship');

        ///\Log::info($result);        
        return (new TrendResult)->trend($result);
            //->previous(50);
            //->prefix('$')
        //return $this->count($request, Model::class);
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            //1 => Nova::__('7 Days'),            
            //7 => '7d.',
           // 30 => '30d'
            /* 
            60 => Nova::__('60 Days'),
            365 => Nova::__('365 Days'),             
            */
            /* 'MTD' => Nova::__('Month To Date'),
            'QTD' => Nova::__('Quarter To Date'),
            'YTD' => Nova::__('Year To Date'), 
            */             
        ];
    }

    /**
     * Determine the amount of time the results of the metric should be cached.
     *
     * @return \DateTimeInterface|\DateInterval|float|int|null
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }
}
