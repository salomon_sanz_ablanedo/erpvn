<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use App\Models\WsOrderPrevision;
use App\Models\WsOrderLine;

class OrderLinesAndPrevisionsObsFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Centro logístico';
    
    public $component = 'select-filter';

    public function apply(Request $request, $query, $value){
        if (!empty($value))
            return $query->where('obs', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $resourceName = $request->get('viaResource');
        $resourceId = $request->get('viaResourceId');
        
        if ($resourceName == 'ws-orders')
            $options = WsOrderLine::select(['obs','obs'])->where('order_id', $resourceId)->get()->pluck('id', 'obs')->toArray();
        else
            $options = WsOrderPrevision::select(['obs','obs'])->get()->pluck('id', 'obs',)->toArray();

        return $options;
    }
}
