<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Classroom;
use Illuminate\Support\Arr;
use App\Models\WsCustomer;

class ArticleTypeFilter extends Filter
{
    public $name = 'TIPO';
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public function apply(Request $request, $query, $value){
        if (!empty($value))
            return $query->where('type', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {

        return [
            'ARTICULO' => 'ARTICULO',
            'COMPONENTE' => 'COMPONENTE',
        ];        
    }
}
