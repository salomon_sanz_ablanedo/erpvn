<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Classroom;
use Illuminate\Support\Arr;
use App\Models\WsCustomer;

class ArticleTimeFilter extends Filter
{
    public $name = 'TIEMPO';
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public function apply(Request $request, $query, $value){
        if (!empty($value))
        {
            if ($value == 'SIN')
            {
                return $query->where(function ($q){
                    $q->whereNull('seconds_per_unity')
                      ->orWhere('seconds_per_unity','=',0);
                });
            }            
            else
            {
                return $query->where(function ($q){
                    $q->whereNotNull('seconds_per_unity')
                      ->Where('seconds_per_unity','!=', 0);
                });
            }
        }
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'CON TIEMPO' => 'CON',
            'SIN TIEMPO' => 'SIN',
        ];        
    }
}
