<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Classroom;
use Illuminate\Support\Arr;

class OrderLinesStatusFilter extends Filter
{
    public $component = 'select-filter';

    public $name = 'Estado';

    public function apply(Request $request, $query, $value){
        if (!empty($value))
            return $query->where('status', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [
            'TODOS'     => '',
            'PENDIENTE' => 'PENDIENTE',
            'EN CURSO'  => 'EN CURSO',
            'HECHO'     => 'HECHO',
            'CANCELADO'     => 'CANCELADO'
        ];

        return $options;
    }
}
