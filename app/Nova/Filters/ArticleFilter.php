<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Classroom;
use Illuminate\Support\Arr;
use App\Models\WsOrderLine;
use App\Models\WsOrderPrevision;

class ArticleFilter extends Filter
{
    public $component = 'select-filter';

    public $name = 'Artículo';

    public function apply(Request $request, $query, $value){
        if (!empty($value))
            return $query->where('article_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $resourceName = $request->get('viaResource');
        $resourceId = $request->get('viaResourceId');

        if ($resourceName == 'ws-orders')
        {
            //$options = WsOrderLine::with(['article'])->select(['article_id','obs'])->where('order_id', $resourceId)->distinct()->get()->pluck('article_id', 'article.name')->toArray();
            $options = WsOrderLine::with('article')->select(['article_id','obs'])->where('order_id', $resourceId)->distinct()->get()
                                    ->mapWithKeys(function ($item) {
                                        return [ $item->article->name . ' | ' . $item->article->reference => $item->article_id];
                                    })->toArray();  
        }
        else
        {
            $options = WsOrderPrevision::with('article')->select(['article_id','obs'])->distinct()->get()->mapWithKeys(function ($item) {
                return [ $item->article->name . ' | ' . $item->article->reference => $item->article_id];
            })->toArray(); 
        }

        return $options;
    }
}
