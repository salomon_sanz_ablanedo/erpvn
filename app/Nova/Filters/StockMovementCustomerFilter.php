<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Classroom;
use Illuminate\Support\Arr;
use App\Models\WsOrderLine;
use App\Models\WsOrderPrevision;
use App\Models\WsCustomer;


class StockMovementCustomerFilter extends Filter
{
    public $name = 'CLIENTE';
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public function apply(Request $request, $query, $customerId){
        
        return $query->whereHas('article', function ($query) use ($customerId) {
            $query->where('customer_id', $customerId);
        });
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = WsCustomer::select(['id','name'])->get()->pluck('id', 'name')->toArray();      

        return $options;
    }
}
