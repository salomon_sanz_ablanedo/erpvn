<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Http\Requests\NovaRequest;
//use AwesomeNova\Cards\FilterCard;
use App\Nova\Filters\OrderLinesStatusFilter;
use App\Nova\Filters\OrderLinesAndPrevisionsObsFilter;
use App\Nova\Filters\ArticleFilter;
use Illuminate\Database\Eloquent\Builder;
use App\Nova\Actions\WsOrderLineCancellation;


class WsOrderLine extends Resource
{
    public static $perPageViaRelationship = 10;
    public static $displayInNavigation = false;
    public static $model = \App\Models\WsOrderLine::class;

    public static $title = 'id';

    public static $with = ['article','order.customer','cancelledBy'];

    public static $searchable = false;    

    public static function label(){
        return __('Trabajos');
    }

    public static function singularLabel(){
        return __('Trabajos');
    }

    public function authorizedToReplicate(Request $request)
    {
        // only allow if is not automatically imported by system
        return $this->order->creator_id != 2;
    }

    public function fields(Request $request)
    {
        //dd($this->order()->get());        
        return [
            //ID::make(__('ID'), 'id')->sortable(), 
            
           /*  Text::make('', function(){
                if ($this->quantity == $this->quantity_supplied)
                    return '🟢';
                else if ($this->quantity_supplied == 0)
                    return '🔴';
                else
                    return '🟡';
            }), */

            /* Badge::make('', function(){
                if ($this->quantity !== $this->quantity_supplied)
                    return 'pendiente';
                else
                    return 'hecho';
            })->map([
                'pendiente' => 'danger',                
                'hecho' => 'success'
            ])->withIcons(), */
            
            BelongsTo::make('Pedido', 'order', \App\Nova\WsOrder::class)->readonly(),

            BelongsTo::make('Artículo', 'article', \App\Nova\WsArticle::class)
                ->searchable()
                ->withSubtitles()
                ->rules('required')
                ->dependsOn('order', function ($field, $request, $formData) {                                       
                    $field->relatableQueryUsing(function ($request, $query) use ($formData) {
                        //\Log::info('$formData->customer_id : '. $formData->customer_id);
                        return $query->where('type', 'ARTICULO')
                                     ->where('customer_id', \App\Models\WsOrder::find($formData['resource:ws-orders'])->customer_id);
                    });
                }),                

            Text::make('', function(){

                $html = ' ';

                if ($this->prioritary)
                    $html = '<b> ⚠️ URGE ⚠️</b>';
                return $html;
            })->exceptOnForms()
                ->withMeta(['textAlign' => 'center'])
                ->asHtml(),

            Number::make('Cantidad','quantity_supplied')
                ->displayUsing(function($value){

                    $green = 'inline-flex items-center whitespace-nowrap min-h-6 px-2 rounded-full text-sm font-bold bg-green-100 text-green-600 dark:bg-green-500 dark:text-green-900';
                    $red = 'inline-flex items-center whitespace-nowrap min-h-6 px-2 rounded-full text-sm font-bold bg-red-100 text-red-600 dark:bg-red-400 dark:text-red-900 mt-1';
                    $yellow = 'inline-flex items-center whitespace-nowrap min-h-6 px-2 rounded-full text-sm font-bold bg-yellow-100 text-yellow-600 dark:bg-yellow-300 dark:text-yellow-800 mt-1';
                    $icon_check = '<span class="ml-1 -mr-1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" width="20" height="20" class="inline-block" role="presentation"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg></span>';
                    $icon_warning = '<span class="mr-1 -ml-1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" width="20" height="20" class="inline-block" role="presentation"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path></svg></span>';

                    if ($this->status == 'CANCELADO')
                    {
                        $result = '<span class="inline-flex items-center whitespace-nowrap min-h-6 px-2 rounded-full text-sm font-bold bg-gray-300 text-white"> x'.$this->quantity.' 🚫</span><br/><span style="font-size:11px;">&nbsp;<b>cancelado</b>&nbsp; por '.$this?->cancelledBy->name . ' el '. date_format(date_create($this->cancelled_at), 'd/m/y H:i') .'</span>';

                        if (!empty($this->cancelled_reason))
                            $result .= '<br><small class="italic">🗨️ &quot;'.$this->cancelled_reason.'&quot;</small>';
                    }
                    elseif ($this->quantity - $this->quantity_supplied == 0)
                        return '<span class="'.$green.'"> x'.$this->quantity.' '.$icon_check.'</span>';
                    else
                        $result = '<span class="'.($this->quantity_supplied > 0? $yellow : $red).'"> x'.$this->quantity - $this->quantity_supplied.' PTE.</span>';

                    if (!empty($this->quantity_supplied) && !$this->status == 'CANCELADO')
                    {
                        $result .= '<br/><span style="font-size:11px;">Total: x'.$this->quantity;

                        if (!empty($this->quantity_orig))
                            $result .= ' (orig. x'.$this->quantity_orig.')';

                        if ($this->quantity_supplied != $this->quantity)
                            $result .= ' | Servido: x'.$this->quantity_supplied;
                    }                    

                    return $result;
                })
                ->readonly()
                ->withMeta(['textAlign' => 'center'])
                ->exceptOnForms()
                ->asHtml(),           
            

            Number::make('Cantidad','quantity')
                ->min(0)
                ->rules('required')
                ->displayUsing(function($value){
                    $result = 'x'.$value;

                    if (!empty($this->quantity_orig))
                        $result .= ' (orig. x'.$this->quantity_orig.')';
                    return $result;
                })->onlyOnForms(),

            

            Text::make('Stock', function(){       
                $stock =  $this->article->stock ?? 0;        
                $result = $stock;

                if ($stock < $this->quantity-$this->quantity_supplied)                
                    $result = '<span style="color:red;">'.$stock.'</span>';
                else
                    $result = '<span class="text-green-600 font-bold">'.$stock.'</span>';
                
                return $result;
            })->asHtml()->onlyOnIndex()
                ->withMeta(['textAlign' => 'center']),

            Boolean::make('Urgente', 'prioritary')->onlyOnForms(),

            Stack::make('Detalles', (function(){
                
                $result = [];

                if (!empty($this->order_ref))
                {
                    $result[] = Line::make('order_ref')
                                    ->displayUsing(function($value){
                                        return 'Ref. Pedido: '.$value;
                                    })
                                    ->asSmall();
                }

                if (!empty($this->obs))
                    $result[] = Line::make('obs')->asSmall()->filterable();

                return $result;
  
            })())
                ->onlyOnIndex(),
                //->filterable(),

            Text::make('Ref. pedido', 'order_ref')
                ->maxlength(32)
                ->hideFromIndex(),

            Text::make('Observaciones', 'obs')
                ->maxlength(40)->enforceMaxlength()
                ->hideFromIndex(),

            //([
           /*  Badge::make('estado','status')->map([
                'PENDIENTE' => 'danger',
                'EN CURSO' => 'warning',
                'HECHO' => 'success'
            ]),
 */
            /* Select::make('estado','status')->options([
                'PENDIENTE' => 'PENDIENTE',
                'EN CURSO' => 'EN CURSO',
                'HECHO' => 'HECHO'
            ])->onlyOnForms(), */
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            //new FilterCard(new OrderLinesStatusFilter)
        ];
    }

    public function filters(Request $request)
    {
        return [
            new OrderLinesStatusFilter,
            new OrderLinesAndPrevisionsObsFilter,
            new ArticleFilter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [            
            WsOrderLineCancellation::make()
                ->confirmText('¿Seguro que quieres cancelar/descancelar esta línea?')               
            //->onlyOnTableRow()            
        ];
    }

    public static function relatableQuery(NovaRequest $request, $query)
    {        
    }
}
