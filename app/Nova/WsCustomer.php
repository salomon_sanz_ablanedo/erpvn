<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Panlatent\CronExpressionDescriptor\ExpressionDescriptor;
use App\Nova\Actions\CatalogImportAction;
use Laravel\Nova\Fields\Boolean;
use Log;

class WsCustomer extends Resource
{
    public static $model = \App\Models\WsCustomer::class;

    public static $title = 'name';

    public static $with = ['orderImporter'];

    public static $search = ['name'];

    public static $group = 'Gestión';

    public static function label(){
        return __('Clientes');
    }

    public static function singularLabel(){
        return __('Cliente');
    }

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Nombre', 'name'),
            Trix::make('Info','info')
                ->alwaysShow(),
            Text::make('Dirección', 'address')->hideFromIndex(),
            Text::make('Prefijo pedidos', 'order_prefix')->maxlength(3)->rules('required'),
            Number::make('Contador pedidos', 'order_count')->readonly(),
            Number::make('Contador albaranes', 'deliverynote_count')->nullable(),
            BelongsTo::make('Importador','orderImporter', \App\Nova\WsOrderImporter::class)->nullable(),
            Textarea::make('Parámetros de importación', 'order_importer_params')->rows(4),            
            Boolean::make('Mostrar posiciones albarán', 'show_delivery_note_positions')->default(true),
            Text::make('Frecuencia de importación', 'order_importer_cron')->displayUsing(function($cron)
                    {
                        if (empty($cron)) return;

                        return (new ExpressionDescriptor($cron, 'es'))->getDescription();                                                
                    })->help(!empty($this->order_importer_cron) ? 'Actual: ' . (new ExpressionDescriptor($this->order_importer_cron, 'es'))->getDescription() : ''),
            
            DateTime::make('Fecha de entrega max. importada', 'date_last_applicable_order_date')->readonly(),
            DateTime::make('Fecha importación últ. pedidos', 'date_last_import_found')->readonly(),
            DateTime::make('Fecha última importación', 'date_last_import_success')->readonly(),
            DateTime::make('Fecha último error de importación', 'date_last_import_error')->readonly(),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [            
            CatalogImportAction::make()->canSee(function(){
                return true;//$this->importable_catalog == true;
            })
                ->extraClasses('bg-gray-50 text-xs')
                ->icon('upload')
                ->iconClasses('mr-5 -ml-2')
                ->onlyOnDetailToolbar()                
                ->standalone()           
            //(new ImportCustomerOrders())
                /*->canSee(function(){
                    return isset($this->order_importer_id);
                })*/
                //->showInline()
            //    ->standalone(),
          /*   (new OrderImportAction())
                ->canRun(function($request, $customer){                       
                    return isset($customer->order_importer_id);
                })
                ->showInline()     */                            
        ];
    }
}
