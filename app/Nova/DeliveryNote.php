<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Str;
use Laravel\Nova\Fields\Currency;

class DeliveryNote extends Resource
{

    public static $with = ['user','lines.article','order','customer'];

    public static $model = \App\Models\DeliveryNote::class;

    public static $title = 'id';

    public static $group = 'Taller';

    //public static $relatableSearchResults = 25;

    public static function label(){
        return __('Albaranes');
    }

    public static function singularLabel(){
        return __('Albarán');
    }


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        //'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),            
            Text::make('Referencia', 'reference')->readonly(),
            Stack::make('Pedido', [                
                BelongsTo::make('Pedido', 'order', \App\Nova\WsOrder::class),
                //Line::make('Pedido', 'reference')->asHeading(),                    
                Line::make('Pedido', function(){
                    return date('d/m/Y', strtotime($this->order->date));
                })->asSmall()
            ])->onlyOnIndex(),
            
            BelongsTo::make('Cliente', 'customer', \App\Nova\WsCustomer::class)
                ->filterable()
                ->sortable()
                ->hideFromIndex(),
            Date::make('Fecha', 'date')                         
                ->sortable()
                ->filterable(),
            HasMany::make('Lineas', 'lines', \App\Nova\DeliveryNoteLine::class),
            Stack::make('items', $this->getLinesForList())
                ->onlyOnIndex(),
            Number::make('Portes', 'shipping_costs')->step(1)->nullable(),
            Boolean::make('Mostrar firma', 'show_signature')->help('Mostrar como firmado al imprimir el albarán')->hideFromIndex(),
            Text::make('')->displayUsing(function(){
                return '<a href="/printdeliverynote/'.$this->id.'" target="_blank" style="text-decoration-line: underline;">🖨️ imprimir</a>';
            })
                ->onlyOnIndex()
                ->asHtml(),
            BelongsTo::make('Creado por', 'user', \App\Nova\User::class)
                ->readonly()
                ->filterable(),
            /* Avatar::make('Usuario')->thumbnail(function(){
                    return $this->user?->getFirstMediaUrl('avatar', 'thumb');                
                })->hideWhenCreating()
                  ->hideWhenUpdating(), */

                  /* ->filled(function(NovaRequest $request, $model){
                    $model->user_id = \Auth::user()->id;
                }) */
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }


    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            Action::openInNewTab('Imprimir', function ($delivery_note) {
                return route('print.delivery_note', $delivery_note->id);
            })
                ->showInline()
                ->sole()
                ->withoutConfirmation(),                       
        ];
    }

    public function getLinesForList()
    {
        $result = [];

        $this->lines->each(function($line) use(&$result){

            $result[] = Line::make('reference')->resolveUsing(function () use ($line) {
                return " x " .$line->quantity. ' | <span class="text-xs" title="'.$line->article->name.'">'. Str::limit($line->article->name, 40, '...') . '</span> | <span class="text-xs">'. $line->article->reference ?? '</span>';                
            })->asSmall()
            ->asHtml();
        });

        return $result;
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }
}
