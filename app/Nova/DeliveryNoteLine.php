<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Http\Requests\NovaRequest;
//use AwesomeNova\Cards\FilterCard;
use App\Nova\Filters\OrderLinesFilter;

//use Wehaa\Liveupdate\Liveupdate;

class DeliveryNoteLine extends Resource
{
    public static $perPageViaRelationship = 10;
    public static $model = \App\Models\DeliveryNoteLine::class;
    public static $displayInNavigation = false;
    public static $title = 'id';    
    public static $with = ['article'];
    public static $searchable = false;

    public static function label(){
        return __('Lineas de albarán');
    }

    public static function singularLabel(){
        return __('Lineas de albarán');
    }

    public function fields(Request $request)
    {
        return [
            //ID::make(__('ID'), 'id')->sortable(),
            /*
            Number::make('Cantidad','quantity')
                ->min(0)
                ->rules('required')
                ->displayUsing(function($value){
                    return 'x'.$value;
            }), */
            BelongsTo::make('Artículo', 'article', \App\Nova\WsArticle::class)
                ->rules('required'),            

            BelongsTo::make('Albarán', 'delivery', \App\Nova\DeliveryNote::class),
            
            Number::make('Cantidad','quantity')->displayUsing(function($value){
                return 'x'.$value;
            }),            
            
            Stack::make('Detalles', (function(){
                
                $result = [];

                if (!empty($this->order_ref))
                {
                    $result[] = Line::make('order_ref')
                                    ->displayUsing(function($value){
                                        return 'Ref. Pedido: '.$value;
                                    })
                                    ->asSmall();
                }

                if (!empty($this->obs))
                    $result[] = Line::make('obs')->asSmall();

                return $result;
  
            })())->onlyOnIndex(),
            
            Text::make('Ref. pedido', 'order_ref')
                ->maxlength(32)
                ->hideFromIndex(),            

            Text::make('Observaciones', 'obs')
                ->maxlength(40)->enforceMaxlength()
                ->hideFromIndex(),

            Number::make('Nº pallet', 'pallet_number')                
                ->nullable()
                ->min(1)
                ->step(1)                
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            //new FilterCard(new OrderLinesFilter)
        ];
    }

    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [            
            //->onlyOnTableRow()            
        ];
    }

    public static function relatableQuery(NovaRequest $request, $query)
    {
        return $query->orderBy('due_date', 'ASC');
    }
}
