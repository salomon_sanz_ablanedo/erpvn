<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Number;
use App\Services\StockService;

class FixStockMovementArt2Comps extends Action
{
    use InteractsWithQueue, Queueable;

    public $name = 'Mvt. correctivo: convertir a componentes';    

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        StockService::addMovement($models[0]->id, abs($fields['quantity']) * -1);
        StockService::setComponentsMovements($models[0], abs($fields['quantity']));        
    }

    /**
     * Get the fields available on the action.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            Number::make('Cantidad','quantity')
                ->rules('required')
                ->min(1)
                ->step(1)
                ->help('La cantidad que introduzcas se restará del stock del artículo y se dará como entrada al stock de componentes'),
        ];
    }
}
