<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class DownloadSheetOrder extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $orders)
    {
        foreach($orders as $data)
        {
            $pdf = PDF::loadView('reports.marks', $data)
                ->setPaper('A4')
                //->setOption('page-size','A4')
                ->setOption('zoom','1.5')
                //->setOption('dpi', 72)
                ->setOption('margin-bottom', 0)
                ->setOption('margin-top', 0)
                ->setOption('margin-left', 0)
                ->setOption('margin-right', 0);

            $local_path = 'storage/'.Str::slug($data['alumn']->name. ' ' .$data['alumn']->surname).'.pdf';

            if (file_exists($local_path))
                unlink($local_path);
            $pdf->save(public_path($local_path));
            $output_files[]=$local_path;
        }

        if (count($output_files) == 1)
        {
            return Action::download(url($local_path), basename($local_path));
        }
        else
        {
            $zip_file = 'storage/notas_'.uniqid().'.zip';
            $zip = new ZipArchive();
            $zip->open(public_path($zip_file), ZipArchive::CREATE | ZipArchive::OVERWRITE);

            foreach($output_files as $file)
                $zip->addFile($file);

            $zip->close();
            return Action::download(url($zip_file), basename($zip_file));
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields(\Laravel\Nova\Http\Requests\NovaRequest $request)
    {
        return [];
    }
}
