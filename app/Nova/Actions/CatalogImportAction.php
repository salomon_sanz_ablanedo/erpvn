<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Brightspot\Nova\Tools\DetachedActions\DetachedAction;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Date;
use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use App\Models\WsCustomer;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Jobs\OrderImporters\concerns\ReportResume;
use Exception;

class CatalogImportAction extends DetachedAction
{
    use InteractsWithQueue, Queueable;

    public $showOnIndex = true;
    public $showOnDetail = false;

    public $title = 'Importar catálogo desde fichero';

    public function label()
    {
        return __('Importar catálogo');
    }

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $file = storage_path('app/'.request()->file('*')[0]['uploadedfile'][0]->store('orderimports','local'));
        $customerId = last((explode('/', request()->headers->get('referer'))));                    

        switch($customerId)
        {
            case 3:
                $job = '\\App\\Jobs\\CatalogImporters\\IndexCatalogImporter';
            break;

            default:
                throw new Exception('No se ha encontrado un importador para el cliente seleccionado.');
        }

        $result = (new $job(['file' => $file]))->handle();

        //Log::info($result);

        return Action::message($result .PHP_EOL . PHP_EOL . '(Refresca la página para ver lo nuevo importado)');
        
        //if ($valid)
            //return Action::message(nl2br((string) $result) .PHP_EOL . PHP_EOL . '(Refresca la página para ver lo nuevo importado)');
        //else
        //    throw new Exception('El documento no se ha podido procesar. Motivo: el fichero no tiene una estructura de datos conocida.');
    }

    /**
     * Get the fields available on the action.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            Files::make('Fichero','uploadedfile')
        ];
    }

    public function importIndex(){

    }
}
