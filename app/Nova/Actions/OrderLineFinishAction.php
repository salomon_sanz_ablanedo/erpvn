<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Brightspot\Nova\Tools\DetachedActions\DetachedAction;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use App\Models\DeliveryNote;
use App\Models\DeliveryNoteLine;
use App\Services\OrderService;
use App\Services\StockService;
use Illuminate\Support\Facades\Log;
use Laravel\Nova\Http\Requests\ActionRequest;
use Illuminate\Support\Facades\DB;
use Exception;
use Laravel\Nova\Fields\Boolean;

class OrderLineFinishAction extends DetachedAction
{
    use InteractsWithQueue, Queueable;    


    public $name = 'Crear albarán';

    public $showOnIndex = false;
    public $showOnDetail = true;
    public $orderId;

    /* final public function __construct($orderId)
    {
        //dd($request);
        Log::info($orderId);
    } */

    public function label()
    {
        return __('Crear Albarán');
    }

    public function handle(ActionFields $fields, Collection $models)
    {
        try
        {
            if (count($models) > 0)
                $order = $models[0];
            else
            {
                $orderId = last((explode('/', request()->headers->get('referer'))));                    
                $order = WsOrder::find($orderId);        
            }
            
            DB::transaction(function() use($order, $fields){                

                $delivery = DeliveryNote::create([
                    'date'        => date('Y-m-d  H:i:s'),
                    'order_id'    => $order->id,
                    'customer_id' => $order->customer_id,
                    'user_id'     => \Auth::user()->id,
                    'shipping_costs' => $fields['shipping_costs'],
                    'show_signature' => $fields['show_signature'],
                ]);
                
                $toInsert = [];                             
                
                $index = 0;
        
                foreach($fields->toArray() as $article_id=>$quantity)
                {
                    $index++;

                    if ($index < 3 || $quantity == 0) continue; // skip "portes" or empty

                    $line_id = explode('_', $article_id)[1];
                    
                    $line = WsOrderLine::find($line_id);

                    $toInsert[] = [
                        'delivery_id' => $delivery->id,
                        'article_id'  => $line->article_id,
                        'quantity'    => $quantity,
                        'obs'         => $line->obs,
                        'order_ref'   => $line->order_ref,
                        'prioritary'  => $line->prioritary
                    ]; 
                    
                    StockService::addMovement($line->article_id, $quantity*-1);                    
                }                                

                DeliveryNoteLine::insert($toInsert);
                OrderService::instance()->updateDeliveredUnits($order);
            });            
        
            return Action::message('It worked!');
            /* 
            return DetachedAction::push('/resources/delivery-notes/'.$delivery->id, [
                'viaResource' => 'ws-orders',
                'viaResourceId' => $$order->id,
                //'viaRelationship' => 'ws-order-lines'
            ]);     
            */
        }catch(Exception $e)
        {
            Log::error($e->getMessage());
            return Action::danger('Ha ocurrido un error, inténtalo de nuevo: '.$e->getMessage());
        }                                            
    }

    public function fields(\Laravel\Nova\Http\Requests\NovaRequest $request)
    {
        $fields = [];
        $actionRequest = $this->actionRequest ?? app(ActionRequest::class);

        //Log::debug($request->resourceId);
        $resourceId = last((explode('/', $request->headers->get('referer'))));

        $lines = WsOrderLine::with('article')
                    ->where([
                        ['order_id', $resourceId],
                        ['quantity', '!=', 'quantity_supplied'],
                        ['status', '!=', 'CANCELADO']
                    ])
                    ->get();

        $fields[] = Number::make('Portes', 'shipping_costs')->step(1)->nullable();
        $fields[] = Boolean::make('Mostrar firma', 'show_signature')->help('Mostrar como firmado al imprimir el albarán');

        $lines->each(function($l) use(&$fields){
            $max = $l->quantity - $l->quantity_supplied;
            
            if ($max > 0)
            {
                $fields[] = Number::make($l->article->name . PHP_EOL .$l->article->reference, 'article_'.$l->id)
                    ->min(0)
                    ->max($max)
                    ->default($max)
                    ->help((function($l){
                        $text = 'Cant. orig: '.$l->quantity .' | Servida: '.$l->quantity_supplied;

                        $text .= ' | Stock: '.$l->article->stock;

                        if (!empty($l->order_ref))
                            $text .= ' | Pedido: '.$l->order_ref;
                        
                        if (!empty($l->obs))
                            $text .= ' | '.$l->obs;

                        return $text;
                    })($l));            
            }
        });

        //$fields[] = Text::make('asdf','asdf');
                
        return $fields;        
    }
}
