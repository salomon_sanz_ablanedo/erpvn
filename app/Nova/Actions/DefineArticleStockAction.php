<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Number;
use App\Services\StockService;

class DefineArticleStockAction extends Action
{
    use InteractsWithQueue, Queueable;

    public $name = 'Definir stock';

    public function handle(ActionFields $fields, Collection $arts)
    {
        foreach($arts as $art)
            StockService::updateStock($art->id, $fields->quantity);
    }

    public function fields(\Laravel\Nova\Http\Requests\NovaRequest $request)
    {
        return [
            Number::make('Cantidad','quantity')
                ->min(0)
                ->step(1),
        ];
    }
}
