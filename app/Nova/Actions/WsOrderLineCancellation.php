<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Actions\ActionResponse;
use App\Services\OrderService;
use Auth;
use Laravel\Nova\Fields\Text;

use Exception;

class WsOrderLineCancellation extends Action
{
    use InteractsWithQueue, Queueable;

    public $name = 'Cancelar/Descancelar línea';
    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model)
        {
            if ($model->status == 'CANCELADO')            
            {
                $model->status = 'PENDIENTE';
                $model->cancelled_by = null;
                $model->cancelled_at = null;
                $model->cancelled_reason = null;
            }
            else
            {
                if ($model->quantity_supplied > 0)
                    throw new Exception('Esta linea tiene líneas albaranadas ya, no se puede cancelar');
                
                $model->status = 'CANCELADO';                
                $model->cancelled_by = Auth::user()->id;
                $model->cancelled_at = $model->freshTimestamp();
                $model->cancelled_reason = $fields->cancelled_reason;
            }



            $model->save();
        }

        if (!empty($models))          
            OrderService::instance()->updateOrderStatus($models[0]->order);

        return ActionResponse::message('Acción ejecutada correctamente');
    }

    /**
     * Get the fields available on the action.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            Text::make('Motivo de cancelación','cancelled_reason')
                ->maxlength(128)
                ->help('Rellena sólo esta información si es para cancelar'),
        ];
    }
}
