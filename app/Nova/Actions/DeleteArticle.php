<?php

namespace App\Nova\Actions;

use Illuminate\Support\Collection;
use Laravel\Nova\Actions\DestructiveAction;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Http\Requests\NovaRequest;
use DB;
use App\Models\WsOrderLine;
use App\Models\WsOrderPrevision;
use App\Models\WsArticle;
use App\Models\WsStockMovement;
use Laravel\Nova\Actions\ActionResponse;
use Exception;

class DeleteArticle extends DestructiveAction
{
    public $name = 'Borrado total';

    public $confirmText = 'Se borrarán las previsiones, lineas de pedido, movimientos de stocks, etc. Si tiene albaranes no se borrará. ¿Estás seguro?';
    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {        
        try
        {            
            $article_id = $models[0]->id;            

            DB::transaction(function() use($article_id){
                WsOrderPrevision::where('article_id', $article_id)->delete();
                WsOrderLine::where('article_id', $article_id)->delete();
                WsArticle::find($article_id)->delete();
                WsStockMovement::where('article_id', $article_id)->delete();
            });

            return ActionResponse::message('Artículo borrado correctamente');
        }
        catch(Exception $e)
        {
            return ActionResponse::danger('El artículo no se puede borrar, posee albaranes');
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [];
    }
}
