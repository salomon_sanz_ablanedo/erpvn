<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Brightspot\Nova\Tools\DetachedActions\DetachedAction;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Date;
use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use App\Models\WsCustomer;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Jobs\OrderImporters\concerns\ReportResume;
use Exception;

class OrderImportAction extends DetachedAction
{
    use InteractsWithQueue, Queueable;

    public $showOnIndex = true;
    public $showOnDetail = false;

    public $title = 'Importar pedidos desde fichero';

    public function label()
    {
        return __('Importar pedidos');
    }

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $file = storage_path('app/'.request()->file('*')[0]['uploadedfile'][0]->store('orderimports','local'));

        $customers = WsCustomer::with('orderImporter')->whereNotNull('order_importer_id')->get();                        
        
        $result = new ReportResume();  
        $valid = false; 
        
        foreach($customers as $customer)
        {
            $params = array_merge([
                'customerId' => $customer->id
            ], json_decode($customer->order_importer_params, true) ?? []);     

            $class = '\\App\\Jobs\OrderImporters\\'.$customer->orderImporter->job_class;
            $importer = new $class($customer, $params ?? null);

            //Log::info(class_basename($class).' isValidExtension: '.($importer->isValidExtension($file) ? 'true' : 'false'));
            // $importer->isValidExtension($file) && Log::info(class_basename($class). ' isFileProcessable: '.($importer->isFileProcessable($file) ? 'true' : 'false'));

            if (!$importer->isValidFile($file)) continue;
            
            if ($importer->isFileIgnorable($file))
                $result->addIgnored();
            else
            {
                $importer->setFiles($file);
                $r = $importer->import();                
                $result->addResume($r);
            }
            //$r .= $customer->name .': true'.PHP_EOL;            
        }

        //if ($valid)
            return Action::message(nl2br((string) $result) .PHP_EOL . PHP_EOL . '(Refresca la página para ver lo nuevo importado)');
        //else
        //    throw new Exception('El documento no se ha podido procesar. Motivo: el fichero no tiene una estructura de datos conocida.');
    }

    /**
     * Get the fields available on the action.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            Files::make('Fichero','uploadedfile')
        ];
    }
}
