<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Brightspot\Nova\Tools\DetachedActions\DetachedAction;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use App\Models\DeliveryNote;
use App\Models\DeliveryNoteLine;
use App\Services\OrderService;
use App\Services\StockService;
use Laravel\Nova\Http\Requests\ActionRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;


class DefineArticlesStockBtOrderAction extends DetachedAction
{
    use InteractsWithQueue, Queueable;    

    public $name = 'Añadir stocks';

    public $showOnIndex = false;
    public $showOnDetail = true;
    public $orderId;

    /* final public function __construct($orderId)
    {
        //dd($request);
        Log::info($orderId);
    } */

    public function label()
    {
        return __('Añadir stocks');
    }

    public function handle(ActionFields $fields, Collection $models)
    {
        try
        {
            DB::transaction(function() use($fields)
            {
                foreach($fields->toArray() as $article_str=>$quantity)
                {                                             
                    if (empty($quantity) || $quantity == 0) continue;

                    $article_id = explode('_', $article_str)[1];                                        

                    StockService::addMovement($article_id, $quantity);                    
                }
            });         

            return Action::message('It worked!');
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            return Action::danger('Ha ocurrido un error, inténtalo de nuevo: '.$e->getMessage());
        }    

    }

    public function fields(\Laravel\Nova\Http\Requests\NovaRequest $request)
    {
        $fields = [];
        $actionRequest = $this->actionRequest ?? app(ActionRequest::class);

        //Log::debug($request->resourceId);
        $resourceId = last((explode('/', $request->headers->get('referer'))));

        $lines = WsOrderLine::with('article')
                    ->where([
                        ['order_id', $resourceId],
                        ['quantity', '!=', 'quantity_supplied'],
                        ['status', '!=', 'CANCELADO']
                    ])
                    ->get();

        $lines->each(function($l) use(&$fields){
            $max = $l->quantity - $l->quantity_supplied;
            
            if ($max > 0)
            {
                $fields[] = Number::make($l->article->name . PHP_EOL .$l->article->reference, 'article_'.$l->article->id)
                    ->min(0)
                    //->max($max)
                    ->default($max)
                    ->help((function($l){
                        $text = 'Cant. orig: '.$l->quantity .' | Servida: '.$l->article->id;

                        $text .= ' | Stock: '.$l->article->stock;

                        if (!empty($l->order_ref))
                            $text .= ' | Pedido: '.$l->order_ref;
                        
                        if (!empty($l->obs))
                            $text .= ' | '.$l->obs;

                        return $text;
                    })($l));
            }
        });

        return $fields;
    }
}
