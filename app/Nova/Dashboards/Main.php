<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Cards\Help;
use Laravel\Nova\Dashboards\Main as Dashboard;
use Coroowicaksono\ChartJsIntegration\StackedChart;

use App\Nova\Metrics\OrdersTotalWorktime;
use App\Nova\Metrics\OrdersDeliveredAtTimePercent;
use App\Nova\Metrics\OrdersWorktimeByCustomer;
use App\Nova\Metrics\OrdersWorktimeByDay;
use App\Nova\Metrics\PrevisionsTotalWorktime;
use App\Nova\Metrics\PrevisionsWorktimeByDays;
use DB;
use Carbon\Carbon;

class Main extends Dashboard
{
    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */

     public function name()
     {
         return 'Estadísticas';
     }

    public function cards()
    {
        $workByDayByCustomerData = $this->getWorkByDayByCustomerData();

        return [                     
            //PrevisionsTotalWorktime::make()->width('2/3'),
            //new Help,                     
            (new StackedChart())
                ->title('Trabajo pendiente en horas (pedidos + previsiones)')
                ->series($workByDayByCustomerData['series'])
                ->options([
                    'showPercentage' => true,
                    'xaxis' => [
                        'categories' => $workByDayByCustomerData['categories']    
                    ]                    
                ])
                ->withMeta([
                    'id'=>'asdf123213123'
                ])
                ->width('full'),            

            OrdersTotalWorktime::make()->width('1/4'),   

            OrdersDeliveredAtTimePercent::make()->width('1/4'),
            //PrevisionsWorktimeByDays::make()->width('3/4'),
        ];
    }

    public function getWorkByDayByCustomerData()
    {
        $from = Carbon::now()->format('Y-m-d');
        $to = Carbon::now()->add(20, 'day')->format('Y-m-d');

        $query_previsions = 'SELECT ROUND(SUM(a.seconds_per_unity * p.quantity / 60 /60), 1) as total, DATE_FORMAT(p.date_ship, \'%d/%m\') as dateship, c.name as customer_name 
            FROM `ws_order_previsions` p 
            INNER JOIN ws_articles a ON p.article_id=a.id 
            INNER JOIN ws_customers c ON p.customer_id=c.id 
            WHERE p.date_ship >= "'.$from.'" 
            AND p.date_ship <="'.$to.'" 
            GROUP BY date_ship, customer_name 
            ORDER BY date_ship ASC
        ';

        $query_orders = 'SELECT ROUND(SUM(a.seconds_per_unity * l.quantity / 60 /60), 1) as total, DATE_FORMAT(o.date, \'%d/%m\') as dateship, c.name as customer_name 
            FROM `ws_orders` o 
            INNER JOIN ws_order_lines l ON l.order_id=o.id 
            INNER JOIN ws_articles a ON l.article_id=a.id 
            INNER JOIN ws_customers c ON o.customer_id=c.id 
            WHERE o.date >= "'.$from.'" 
            AND o.date <="'.$to.'" 
            AND o.status="PENDING" 
            AND l.quantity != l.quantity_supplied
            GROUP BY date, customer_name 
            ORDER BY date ASC
        ';

        $query_orders_acumulated = 'SELECT ROUND(SUM(a.seconds_per_unity * l.quantity / 60 /60), 1) as total, c.name as customer_name 
            FROM `ws_orders` o 
            INNER JOIN ws_order_lines l ON l.order_id=o.id 
            INNER JOIN ws_articles a ON l.article_id=a.id 
            INNER JOIN ws_customers c ON o.customer_id=c.id 
            WHERE o.date < "'.$from.'" 
            AND o.status="PENDING" 
            AND l.quantity != l.quantity_supplied
            GROUP BY customer_name
        ';

        $result_previsions = DB::select($query_previsions);
        $result_orders = DB::select($query_orders);
        $result_orders_accumulated = DB::select($query_orders_acumulated);

        foreach($result_orders_accumulated as &$acc)
            $acc->dateship = 'acumulado';

        //\Log::info($result_orders_accumulated);

        $result = array_merge($result_orders_accumulated, $result_previsions, $result_orders);

        $distinct_days = [];
        $distinct_customers = []; 
        $series = [];       
        $colors = [
            "#ffcc5c","#91e8e1","#ff6f69","#b088d8","#d8b088", "#88b0d8", "#6f69ff"
        ];

        foreach($result as $row)
        {
            if (!in_array($row->customer_name, $distinct_customers))
                $distinct_customers[] = $row->customer_name;

            if (!isset($distinct_days[$row->dateship]))            
                $distinct_days[$row->dateship] = [];            

             if (!isset($distinct_days[$row->dateship][$row->customer_name]))
                $distinct_days[$row->dateship][$row->customer_name] = $row->total;
        }

        foreach($distinct_days as $date => &$day)
        {
            foreach($distinct_customers as $customer)
            {
                if (!array_key_exists($customer, $day))
                    $day[$customer]=0;
            }            
        }

        foreach($distinct_customers as $index => $customer)
        {
            $serie = [
                'barPercentage' => 0.5,
                'label' => $customer,
                'backgroundColor' => $colors[$index],
                'data'  => []
            ];

            foreach($distinct_days as $d)
                $serie['data'][] = $d[$customer];            

            $series[] = $serie;
        }

        return [
            'series' => $series,
            'categories' => array_keys($distinct_days)
        ];
    }
}
