<?php

namespace App\Nova;

// fields
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Actions\Action;


// actions
use App\Nova\Actions\DefineArticlesStockBtOrderAction;
use App\Nova\Actions\OrderImportAction;
use App\Nova\Actions\OrderLineFinishAction;

// metrics

// filters
use App\Nova\Filters\OrdersStatusFilter;
use App\Nova\Filters\OrdersCustomerFilter;

// other
use Illuminate\Http\Request;
use Carbon\Carbon;
use Laravel\Nova\Http\Requests\NovaRequest;
use Nikans\TextLinked\TextLinked;
use Illuminate\Support\Str;
use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use App\Services\ArticleService;
use AwesomeNova\Cards\FilterCard;


class WsOrder extends Resource
{    
    public static $model = \App\Models\WsOrder::class;

    public  function title(){
        return $this->reference;// . ' (' . date('d/m/y', strtotime($this->date)) . ' - '.$this->customer->name.')';
    }

    /* public static $search = [
        'reference',
    ]; */

    public static $defaultOrder = 'created_at';
    public static $defaultOrderDirection = 'desc';

    public static $globallySearchable = true;
    public static $searchable = false;

    public static $with = [
        'customer'        
    ];

    public static $group = 'Taller';
    public static $tableStyle = 'tight';


    public static function label(){
        return __('Pedidos');
    }

    public static function singularLabel(){
        return __('Pedido');
    }

    public function fields(Request $request)
    {
        return [
            /* TextLinked::make('Pedido', 'id')
                ->link($this)
                ->sortable(), */
                //Avatar::make('Creador', 'creator_id'),
            Stack::make('Pedido', [                
                BelongsTo::make('Cliente', 'customer', \App\Nova\WsCustomer::class),    
                //Line::make('Pedido', 'reference')->asHeading(),                    
                Line::make('Pedido', function(){
                    return $this->reference;
                })->asSmall(),
            ])->onlyOnIndex(),
            
            Text::make('Pedido', 'reference')->readonly(true)->hideFromIndex(),

            BelongsTo::make('Cliente', 'customer', \App\Nova\WsCustomer::class)->hideFromIndex()->filterable(),            

            BelongsTo::make('Creado por', 'creator', \App\Nova\User::class)->readonly(function(){
                return $this->creator_id == 2;
            })->onlyOnForms(),

            Date::make('Fecha entrega', 'date')                
                ->min(Carbon::today())
                ->sortable()
                ->rules('required'),

            Stack::make('Trabajos', $this->getLinesForList())
                ->onlyOnIndex(),

            Textarea::make('Observaciones', 'observations')
                ->rows(4)
                ->alwaysShow(),

            Badge::make('Estado','status')->map([
                'PENDING'   => 'danger',
                'COMPLETED' => 'success'
            ])->filterable()
            ->withIcons(),

            Text::make('Tiempo requerido', function($resource){
                $total = $this->lines->sum(function($line){
                    return ArticleService::getOrderTime($line->article, $line->quantity, false);
                });

                return ArticleService::getOrderTime4Humans(ArticleService::roundTo5($total), true);
            })->readonly(),   

            DateTime::make('Fecha completado', 'date_completed')->readonly(true)->hideFromIndex(),

            Stack::make('Creado por', [
                //Text::make('Pedido', 'reference')
                BelongsTo::make('Creado por', 'creator', \App\Nova\User::class),                    
                Line::make('Hora', 'created_at', function($value){
                    return $value->diffForHumans();
                })->asSmall()
            ]),

            Text::make('')->displayUsing(function(){
                return '<a href="/printorder/'.$this->id.'" target="_blank" style="text-decoration-line: underline;">🖨️ imprimir</a>';
            })
                ->onlyOnIndex()
                ->asHtml(),                    

            Files::make('Ficheros del pedido', 'orderfiles'),
            HasMany::make('Lineas', 'lines', \App\Nova\WsOrderLine::class),
            HasMany::make('Albaranes', 'deliveryNotes', \App\Nova\DeliveryNote::class)
        ];
    }

    public function cards(Request $request)
    {
        return [
            //OrdersWorktimeByCustomer::make(),
            //OrdersWorktimeByDay::make(),
            //new FilterCard(new OrdersStatusFilter())
        ];
    }

    public function filters(Request $request)
    {
        return [
            new OrdersCustomerFilter,
            //new OrdersStatusFilter
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            OrderLineFinishAction::make($request->resourceId)
                ->onlyOnDetail()                
                ->icon('clipboard-check')
                ->standalone(),

            DefineArticlesStockBtOrderAction::make()
                ->onlyOnDetail()
                ->icon('view-grid-add')
                ->standalone(),
                //->sole(),                

            (new OrderImportAction($request->resourceId))
                ->extraClasses('bg-gray-50 text-xs')
                ->icon('upload')
                ->iconClasses('mr-5 -ml-2')
                ->onlyOnIndexToolbar()
                ->standalone(),
                
            Action::openInNewTab('Imprimir', function ($order) {
                return route('print.order', $order->id);
            })
                ->showInline()
                ->sole()
                ->withoutConfirmation(),  
                //->withoutConfirmation()                    
            //>canSee(function(){return true;})
                //->onlyOnDetailToolbar()     
                
                //->onlyInline()
                //->extraClasses('bg-logo text-white hover:black')   
                //->extraClassesWithDefault('bg-danger')        
                //->icon('check-circle')
                //(new ImportCustomerOrders())
                /*->canSee(function(){
                    return isset($this->order_importer_id);
                })*/
                //->showInline()
            //    ->standalone(),
           /*  (new OrderImportAction())
            ->canRun(function($request, $customer){                       
                return isset($customer->order_importer_id);
            }) */            
        ];
    }

    public function getLinesForList()
    {
        $result = [];

        $arts = [];

        // group lines by ref
        $this->lines->each(function($line) use(&$arts)
        {
            if (!isset($arts[$line->article_id]) || $line->status == 'CANCELADO')
            {
                $arts[$line->article_id] = (object) [
                    'article_id' => $line->article_id,
                    'name'       => $line->article->name,
                    'reference'  => $line->article->reference,
                    'quantity'   => 0,
                    'quantity_supplied' => 0,
                    'prioritary' => false,
                    'status'    => $line->status
                ];
            }

            $arts[$line->article_id]->quantity += $line->quantity;
            $arts[$line->article_id]->quantity_supplied += $line->quantity_supplied;

            if ($line->prioritary)
                $arts[$line->article_id]->prioritary = true;
        });

        foreach($arts as $key=>$line)
        {
            $result[] = Line::make('reference')->resolveUsing(function () use ($line) 
            {
                $text = '';

                if ($line->status == 'CANCELADO')                    
                    $text = '🚫 x'.$line->quantity;
                elseif ($line->quantity_supplied == 0)
                    $text = '🔴 x'.$line->quantity;
                elseif ($line->quantity == $line->quantity_supplied)
                    $text = '🟢 x'.$line->quantity;
                else
                    $text = "🟡 x$line->quantity ({$line->quantity_supplied} hecho)";

                $text .= ' | <span class="text-xs" title="'.$line->name.'">'. Str::limit($line->name, 25, '...') . '</span><span class="text-xs"> | '. $line->reference ?? ''.'</span>';

                if ($line->prioritary)
                    $text .= '<span style="color:red;" class="text-xs"><b> ⚠️ URGE ⚠️</b></span>';
                
                if ($line->status == 'CANCELADO')
                    $text .= '&nbsp; <span style="color:white; background-color:#999; font-size:11px;" class="rounded text-xs">&nbsp;<b>cancelado</b>&nbsp;</span>';
                return $text;
            })->asSmall()->asHtml();
        }

        return $result;
    }    
}
