<?php

namespace App\Jobs\OrderImporters;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Carbon\Carbon;
use App\Models\WsOrderPrevision;
use App\Models\WsArticle;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Jobs\OrderImporters\concerns\ImporterFromEmailAttachment;
use App\Services\CustomerParsers\IndexParserService;
use App\Jobs\OrderImporters\concerns\DataImportedManifest;
use App\Jobs\OrderImporters\concerns\DataImportedManifestItem;
use App\Services\ImapService;
use DateTime;
use SebastianBergmann\Type\NullType;

class IndexPdfImporter extends ImporterFromEmailAttachment
{
    public function isFileProcessable($file):bool
    {
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($file);   

        return Str::startsWith($pdf->getPages()[0]->getText(), 'INDEX');
    }

    public function isFileIgnorable($file):bool{
        return false;
    }

    public function importFromFile($file):DataImportedManifest|false
    {                                   
        //$customerId, $dateToSend, $importDate, $items = [])         
        $manifest = new DataImportedManifest($this->customer->id, [], $file);

        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($file);    

        foreach($pdf->getPages() as $page)
        {
            $data = $page->getText();

            preg_match('/\* \*(.*)\*/', $data, $order_ref);           
            preg_match('/(.*)Artículo:/', $data, $article_ref);
            preg_match('/Cantidad:[\r\n](.*)[\r\n]/', $data, $article_name);                              
            preg_match('/Fecha Entrega:(.*),00Cantidad:/', $data, $quantity);         // Fecha Entrega:300,00Cantidad:                           
            preg_match('/[\r\n](.*)Fecha Entrega/', $data, $date_ship);                       
            preg_match('/EXTERNO[\r\n](.*)[\r\n]/', $data, $date_order);    
            preg_match('/[\r\n]BOPE[\r\n]/', $data, $prioritary);                          

            $manifest->addItem(new DataImportedManifestItem(
                $article_ref[1],
                $article_name[1],
                (int) str_replace('.','', $quantity[1]),
                DateTime::createFromFormat('d/m/y H:i:s', $date_ship[1] . ' 00:00:00'),
                DateTime::createFromFormat('d/m/y H:i:s', $date_order[1] . ' 00:00:00'),
                $order_ref[1],
                $file,
                null,
                !empty($prioritary)
            ));            
        }        

        return $manifest;
    }      
    
    static public function validExtensions():array{
        return ['pdf'];
    }

    protected static function getAttachmentOrderDataFilter():callable{
        return function($att){
            return $att->content_type == 'application/pdf' || 
                   $att->content_type == 'application/octet-stream' && Str::endsWith(strtolower($att->name), '.pdf');
        };            
    }

    protected static function getAttachmentOrderFilesFilter():?callable{
        return null;
    }

    // INDEX HAS NO PREVISIONS
    protected function filterItemsForPrevisionCondition():callable{
        return function($item){
            return false;
        };
    }
   
    // INDEX HAS ALL DIRECT ORDERS
    protected function filterItemsForOrderCondition():callable{
        return function($item){
            return true;
        };
    }

    public function middleware(){
        return [new WithoutOverlapping('KYB')];
    }
}
