<?php

namespace App\Jobs\OrderImporters;


use Carbon\Carbon;
use App\Utils\CsvParser;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use App\Jobs\OrderImporters\concerns\ImporterFromFile;
use App\Jobs\OrderImporters\concerns\DataImportedManifest;
use App\Jobs\OrderImporters\concerns\DataImportedManifestItem;
use DateTime, DateTimeZone;
use PhpOffice\PhpSpreadsheet\IOFactory;


class SchneiderCsvImporter extends ImporterFromFile
{
    const FIELDS = [
        'fecha_de_albaran',
        'pedido',
        'pos',
        'referencia',
        'descripcion_del_articulo',
        'fecha_de_entrega',
        'cantidad',
        'centro_logistico'
    ];

    public function isFileProcessable($file):bool
    {
        //$sheet = IOFactory::load($file)->getActiveSheet();
        $sheet = IOFactory::load($file)->getSheet(0);

        $ultimaColumna = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($sheet->getHighestColumn());

        // Obtener los títulos de las columnas
        $cols = [];

        for ($i = '1'; $i <= $ultimaColumna; $i++)
            $cols[] = Str::slug($sheet->getCellByColumnAndRow($i, '1')->getValue(), '_');

        // remove empty columns
        $cols = array_filter($cols);

        $is_processable = empty(array_diff(static::FIELDS, $cols)) && count(static::FIELDS) == count($cols);

        return $is_processable;       
    }

    public function isFileIgnorable($file):bool{
        return false;
    }
    
    public function getFieldIndex($name){
        return array_search($name, static::FIELDS) + 1;
    }

    public function importFromFile($file):DataImportedManifest|false
    {        
        $manifest = new DataImportedManifest($this->customer->id, [], $file);

        $sheet = IOFactory::load($file)->getSheet(0); 

        $items_groups = [];

         // Iterar a través de cada fila de la sheet        
        foreach ($sheet->getRowIterator() as $rowIndex => $row)
        {                        
            if ($rowIndex == 1) continue;
            
            $logistic_center_key = $sheet->getCell([$this->getFieldIndex('centro_logistico'), $rowIndex])->getValue();

            if (empty($logistic_center_key))
                $logistic_center_key = 'PLR';

            $reference = $sheet->getCell([$this->getFieldIndex('referencia'), $rowIndex])->getValue();
            
            $date_of_deliver = $sheet->getCell([$this->getFieldIndex('fecha_de_entrega'), $rowIndex])->getValue();
            $date_ship = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date_of_deliver, config('app.timezone'));
            $date_ship_formatted = $date_ship->format('Y-m-d');
            // ['fecha_de_albaran (0)', 'pedido (1)','pos (2)','referencia (3)','descripcion_del_articulo (4)','fecha_de_entrega (5)','cantidad (6)','centro_logistico (7)']
            //$reference, $name, $quantity, $date, $order_ref = null, $file = null
            //Log::debug($sheet->getCell([$this->getFieldIndex('fecha_de_entrega'), $rowIndex])->getValue());

            $item = new DataImportedManifestItem(
                $reference,   //$reference
                $sheet->getCell([$this->getFieldIndex('descripcion_del_articulo'), $rowIndex])->getValue(), // $name
                (int) $sheet->getCell([$this->getFieldIndex('cantidad'), $rowIndex])->getValue(), // $quantity
                $date_ship, // $date_ship
                DateTime::createFromFormat('d/m/Y H:i:s', date('d/m/Y') . ' 00:00:00'), // $order_ref
                $sheet->getCell([$this->getFieldIndex('pedido'), $rowIndex])->getValue(), // $order
                $file,
                'Centro Log: '.$logistic_center_key,
                false,
                null,
                $date_ship_formatted
            );

            if (!isset($items_groups[$date_ship_formatted]))   
                $items_groups[$date_ship_formatted] = [];

            if (!isset($items_groups[$date_ship_formatted][$logistic_center_key]))        
                $items_groups[$date_ship_formatted][$logistic_center_key] = [];

            if (!isset($items_groups[$date_ship_formatted][$logistic_center_key][$reference]))
                $items_groups[$date_ship_formatted][$logistic_center_key][$reference] = $item;
            else
                $items_groups[$date_ship_formatted][$logistic_center_key][$reference]->quantity += $item->quantity;                               
        }

        foreach($items_groups as $day_group)
        {
            foreach($day_group as $log_center)
            {
                foreach ($log_center as $ref)
                {
                    $manifest->addItem($ref);
                }
            }
        }

        $manifest->items = collect($manifest->items)
                                ->sortBy([                                    
                                    ['obs', 'asc'],
                                    ['prioritary', 'desc'],
                                    ['name', 'asc']                             
                                ])         
                                ->values()
                                ->all();

        return $manifest;
    }

    static public function validExtensions():array{
        return ['csv','xlsx'];
    }

    protected function filterItemsForPrevisionCondition():callable{
        return function($item){                       
            return Carbon::parse($item->date_ship)->gt($this->getOrderDateLimit());            
        };
    }

    protected function getOrderDateLimit(){
        return $this->getNextApplicableDay(Carbon::parse(date('Y-m-d')));
    }
   
    protected function filterItemsForOrderCondition():callable{
        return function($item){            
            $date_ship = Carbon::parse($item->date_ship);
            $latest_applicable_date = $this->getNextApplicableDay(Carbon::parse($this->customer->date_last_applicable_order_date));
            // gt: greater than, lte: menor o igual
            $valid = $date_ship->gt($latest_applicable_date) && $date_ship->lte($this->getOrderDateLimit());            

            /* if ($date_ship->format('Y-m-d') == '2025-01-27')
            {
                Log::info('Valid: '.($valid ? 'si':'no').' | item dateship: '.$item->date_ship->format('Y-m-d H:i:s').' | applicable_date: '.$this->getOrderDateLimit()->format('Y-m-d H:i:s').' | latest_applicable_date: '.$latest_applicable_date->format('Y-m-d H:i:s'));
                Log::info('gt: '.($date_ship->gt($latest_applicable_date) ? 'true':'false').' | lte: '.($date_ship->lte($this->getOrderDateLimit()) ? 'true':'false'));
            } */

            return $valid;
        };
    }

    protected function getNextApplicableDay(Carbon $date){
        // si es viernes, sábado o domingo, se coge pedidos hasta lunes por defecto, si no se coge el día siguiente
        if (in_array($date->dayOfWeek, [Carbon::FRIDAY, Carbon::SATURDAY, Carbon::SUNDAY]))        
            return $date->next(Carbon::MONDAY);
        else
            return $date->addDay();
    }
}
