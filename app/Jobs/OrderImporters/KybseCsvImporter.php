<?php

namespace App\Jobs\OrderImporters;


use Carbon\Carbon;
use App\Models\WsOrderPrevision;
use App\Models\WsArticle;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use App\Services\ImapService;
use Illuminate\Support\Arr;
use Webklex\IMAP\Facades\Client;
use App\Jobs\OrderImporters\concerns\ImporterFromEmailAttachment;

class KybCsvImporter extends ImporterFromEmailAttachment
{    
    const CSV_DATE_FORMAT = 'd/m/Y';    
    
    private $dateToOrder;    

    private function getCSVRows($filter_time = 'equal_or_greater')
    {        
        // "Item Number" => "0304 217 0033"
        // "Description 1" => "SUBCTO.GUIA VASTAGO"
        // "Net Req Qty" => "200"
        // "Ship Date" => "19/10/2022"        


        /*
        "Supplier" => "S0008"
        "Ship-To" => "KSE100"
        "Release ID" => "20221018-001"
        "Purchase Order" => "OSS0008"
        "Purchase Order Line" => "4"
        "Item Number" => "0304 217 0033"
        "Description 1" => "SUBCTO.GUIA VASTAGO"
        "Description 2" => ""
        "Supplier Item" => ""
        "Contact" => ""
        "Cum Received" => "7500"
        "Packing Slip/Shipper" => "105"
        "Interval" => "D"
        "Ship Date" => "19/10/2022"
        "Reference" => ""
        "Q" => "P"
        "Req Qty" => "200"
        "Cum Req Qty" => "7700"
        "Net Req Qty" => "200"
        "Net Weight" => "0,00001"
        "Unit of Measure" => "KG"
        "Volume" => "0"
        "Size UM" => "CM"
        "Location" => "ST0000"
        */

        //$data = str_getcsv(file_get_contents(storage_path('app/ordermanifest/kyb.csv')), ";", '', "\n");     
        $rows      = explode(PHP_EOL, file_get_contents($this->getAttachmentLocalPath()));
        $fields    = explode(';', array_shift($rows));                                
        $finalrows = [];                

        foreach($rows as $key=>$csv)
        {
            if (empty($csv)) continue;

            $row = array_combine($fields, explode(';', $csv));

            if ($row['Ship Date'] == 'Prior') continue;

            $shipdate = Carbon::createFromFormat(static::CSV_DATE_FORMAT, $row['Ship Date']);

            // discards rows of past date
            if ($filter_time == 'equal_or_greater' && $shipdate->lt($this->dateToOrder))
                continue;
            
            $finalrows[] = $row;
        }        

        return $finalrows;
    }    

    private function calcDateToOrder(){
        $this->dateToOrder = Carbon::createFromFormat(static::CSV_DATE_FORMAT, $this->getCSVRows('all')[0]['Release Date']);        
    }
    
    private function extractReferences()
    {
        // Supplier	Ship-To	Release ID	Purchase Order	Purchase Order Line	Item Number	Description 1	Description 2	Supplier Item	Contact	Release Date	Buyer	Credit Terms	Description	In Transit Qty	Receipt Date	Receipt Quantity	Revision	Cum Received	Packing Slip/Shipper	Interval	Ship Date	Reference	Q	Req Qty	Cum Req Qty	Net Req Qty	Net Weight	Unit of Measure	Volume	Size UM	Location        
        $rows = $this->getCSVRows();        
                
        $references = [];

        foreach($rows as $row)
        {                    
            if (!isset($references[$row['Item Number']]))
                $references[$row['Item Number']] = $row['Description 1'];           
        }

        return $references;
    }
}
