<?php

namespace App\Jobs\OrderImporters;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Carbon\Carbon;
use App\Models\WsOrderPrevision;
use App\Models\WsArticle;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Jobs\OrderImporters\concerns\ImporterFromEmailAttachment;
use App\Services\CustomerParsers\KybseParserService;
use App\Jobs\OrderImporters\concerns\DataImportedManifest;
use App\Jobs\OrderImporters\concerns\DataImportedManifestItem;
use App\Services\ImapService;
use DateTime;
use SebastianBergmann\Type\NullType;

class KybsePdfImporter extends ImporterFromEmailAttachment
{
    public function isFileProcessable($file):bool
    {
        $kybParser = KybseParserService::INSTANCE();
        $kybParser->loadFile($file);        
        
        // important to avoid "SUPPLIER SCHEDULE -"        
        if ($this->customer->id == 1)
            return str_contains($kybParser->getPages(0), 'UPDATE SCHEDULE - KYBSE');
        else        
            return str_contains($kybParser->getPages(0), 'UPDATE SCHEDULE - KAMS');        
    }

    public function isFileIgnorable($file):bool{
        $kybParser = KybseParserService::INSTANCE();
        $kybParser->loadFile($file);                
        return str_contains($kybParser->getPages(0), 'SUPPLIER SCHEDULE -');
    }

    public function importFromFile($file):DataImportedManifest|false
    {
        $kybParser = KybseParserService::INSTANCE();
        $kybParser->loadFile($file);
        $header = $kybParser->getHeader();        
                
        //$customerId, $dateToSend, $importDate, $items = [])         

        $manifest = new DataImportedManifest($this->customer->id, [], $file);

        //dd($kybParser->getItems(5));
        //dd($kybParser->getItems(2));
        //dd($kybParser->getItems(2));
        
        foreach($kybParser->getPages() as $pageIndex=>$page)
        {
            $items = $kybParser->getItems($pageIndex);

            foreach($items as $item)
            {
                //$reference, $name, int $quantity, $date_ship, $date_order, $order_ref = null, $file = null, $obs = null, $prioritary = false, $quantity_orig = null)
                $manifest->addItem(new DataImportedManifestItem(
                    $item['item'], 
                    null,
                    (int) str_replace('.', '', $item['quantity']), 
                    DateTime::createFromFormat('d/m/y H:i:s',  $item['date'] . ' 00:00:00'),
                    DateTime::createFromFormat('d/m/Y H:i:s', $header['Fecha']. ' 00:00:00'),
                    $header['Purchase Order'],
                    $file,
                    null,
                    $item['prioritary']
                ));
            }
        }

        return $manifest;
    }        

    static public function validExtensions():array{
        return ['pdf'];
    }

    protected static function getAttachmentOrderDataFilter():callable{
        return function($att){
            return $att->content_type == 'application/pdf' || 
                   $att->content_type == 'application/octet-stream' && Str::endsWith(strtolower($att->name), '.pdf');
        };            
    }

    protected static function getAttachmentOrderFilesFilter():?callable{
        return null;
    }

    protected function filterItemsForPrevisionCondition():callable{
        return function($item){
            return Carbon::parse($item->date_ship)->gt(Carbon::parse($item->date_order));
        };
    }
   
    protected function filterItemsForOrderCondition():callable{
        return function($item){                        
            return Carbon::parse($item->date_ship)->isSameDay(Carbon::parse($item->date_order));
        };
    }

   /*  private function getEmailMessage()
    {
        $client = Client::account('gmail');

        $client->connect();     
                
        # https://www.php-imap.com/api/query        
        $messages = $client->getFolder('INBOX')->messages()->from($this->emailFrom)->unseen()->get();
                    
        $message = Arr::first($messages, function($m){
            return $m->hasAttachments() && Arr::first($m->getAttachments()->toArray(), function($att){
                return $att->content_type == 'text/csv';
            }, false);
        });        

        return $message;                
    } */

    public function middleware(){
        return [new WithoutOverlapping('KYB')];
    }
}
