<?php

namespace App\Jobs\OrderImporters;


use Carbon\Carbon;
use App\Utils\CsvParser;
use Illuminate\Support\Str;
use App\Jobs\OrderImporters\concerns\ImporterFromEmailAttachment;
use App\Jobs\OrderImporters\concerns\DataImportedManifest;
use DateTime, DateTimeZone;


class IndexCsvImporter extends ImporterFromEmailAttachment
{
    static public function validExtensions():array{
        return ['pdf'];
    }
    
    protected static function getAttachmentOrderDataFilter():callable{
        return function($att){
            //echo $att->name . ' => '.$att->content_type.PHP_EOL;            
            return $att->content_type == 'text/csv' || 
                   $att->content_type == 'application/octet-stream' && Str::endsWith(strtolower($att->name), '.csv');
        };
    }

    protected static function getAttachmentOrderFilesFilter():callable{
        return function($att){
            //echo $att->name . ' => '.$att->content_type.PHP_EOL;            
            return $att->content_type == 'application/pdf' || 
                   $att->content_type == 'application/octet-stream' && Str::endsWith(strtolower($att->name), '.pdf');
        };
    }

    public function importFromFile($file):DataImportedManifest|false
    {
        $items = CsvParser::getCSVParsed($file, ';');

        if (empty($items[array_key_last($items)]['referencia']))
            array_pop($items);

        //$emailDate = $email->get('date')[0]->format('Y-m-d');

        // get timezone, is necessary to specify app.timezone because if not, gets a previous day
        $dateToSend = DateTime::createFromFormat('U', strtotime('next Friday'))->setTimezone(new DateTimeZone(config('app.timezone')));        

        $manifest = new DataImportedManifest($this->customer->id, $dateToSend);

        // Antiguo: "REFERENCIA","Num. OFs","MESES DISP.","UNDS. CAJA","Nº DE CAJAS","TIPO PALET","CANT. PALETS","Nº CAJA","Nº CAJAS REAL","Nº ANCLAJES","AUTOSERVICIO","CANTIDAD PZAS. REAL","DENOMINACIÓN","Nº ORDEN" // remove the last element (is a resume)
        // Nuevo  : "REFERENCIA", "DESCRIPCION", "Num. OFs", "MESES DISP.", "UNDS. CAJA", "Nº DE CAJAS", "TIPO PALET", "CANT. PALETS", "Nº CAJA", "Nº CAJAS REAL", "LIBRE SERVICIO", "CAJA PROFESIONAL", "Nº ORDEN"
        // Nuevo 2: "REFERENCIA", "DENOMINACION", "Num. OFs", "MESES DISP.", "UNDS. CAJA", "Nº DE CAJAS", "TIPO PALET", "CANT. PALETS", "Nº CAJA", "Nº CAJAS REAL", "Nº ANCLAJES", "BOLSAS", "CAJA PROFESIONAL UNDS INICIAL", "Nº ORDEN"        
        
        foreach($items as $item)
        {            
            if ($item['meses_disp'] == 'BOPE')
                $date = $dateToSend;
            else
            {
                $daysToSume = floor(30 * floatval(str_replace(',','.',$item['meses_disp'])));

                $date = Carbon::now()->addDays($daysToSume);

                // si sábado o domingo, pasa al lunes
                if ($date->dayOfWeek == 0)
                    $date = Carbon::now()->addDays($daysToSume+1);
                elseif ($date->dayOfWeek == 6)
                    $date = Carbon::now()->addDays($daysToSume+2);
            }
            
            $quantity = intval($item['caja_profesional_unds_inicial']);//libre_servicio,cantidad_pzas_real
                     
            $manifest->addItem($item['referencia'], $quantity, DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y-m-d') . ' 00:00:00'), $item['denominacion']);
        }        

        return $manifest;
    }
}
