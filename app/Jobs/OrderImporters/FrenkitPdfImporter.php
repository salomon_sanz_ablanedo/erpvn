<?php

namespace App\Jobs\OrderImporters;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Carbon\Carbon;
use App\Models\WsOrderPrevision;
use App\Models\WsArticle;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Jobs\OrderImporters\concerns\ImporterFromEmailAttachment;
use App\Services\CustomerParsers\IndexParserService;
use App\Jobs\OrderImporters\concerns\DataImportedManifest;
use App\Jobs\OrderImporters\concerns\DataImportedManifestItem;
use App\Services\ImapService;
use DateTime;
use SebastianBergmann\Type\NullType;

class FrenkitPdfImporter extends ImporterFromEmailAttachment
{
    public function isFileProcessable($file):bool
    {
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($file);           
        return Str::contains($pdf->getPages()[0]->getText(), 'Frenkit, S.L');
    }

    public function isFileIgnorable($file):bool{
        return !$this->isFileProcessable($file);
    }

    public function importFromFile($file):DataImportedManifest|false
    {        
        //$customerId, $dateToSend, $importDate, $items = [])         
        $manifest = new DataImportedManifest($this->customer->id, [], $file);

        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($file);      

        $items_times = [];

        $date_ship = $this->getDateShip();

        foreach($pdf->getPages() as $page_index => $page)
        {
            $data = $page->getText();
            $data = str_replace("\r\n", "\n", $data); // Asegúrate de que los saltos de línea se manejen de forma uniforme            

            if (str_contains($data, 'Ficha de Envasado'))
            {
                // order_ref no se usa para sacar info tiempos
                //preg_match('/Ficha de Envasado[\r\n](.*)Orden Envasado :/', $data, $order_ref);
                $items_raw = preg_split('/U\.CAJA :\d+[\r\n]/', preg_split('/Tipo de Envase[\r\n]/', $data)[1]);
                array_pop($items_raw); // remove the last element (is a resume)                      

                //if ($page_index > 0) dd($items_raw);

                foreach($items_raw as $item)
                {
                    //dd($item);
                    preg_match('/^(?:[^+]*\+.(.*)|.*\s.(.*))$/', preg_split('/[\r\n]/', $item)[0], $article_ref);                                      
                    preg_match('/(.*?)(?=\t|\+\d|\+| \S+$|$)/', preg_split('/[\r\n]/', $item)[0], $name);                    
                    
                    $article_ref = !empty($article_ref[1]) ? $article_ref[1] : $article_ref[2];
                    $name = trim($name[1]);

                    preg_match('/[\t](.*)Tiempo Estimado/', $item, $total_time_in_minutes);
                    preg_match('/D:\s*(\d+)\s*[\r\n]/', $item, $quantity);
                    $seconds_per_unity = (int) $total_time_in_minutes[1] * 60 / (int) $quantity[1];

                    $items_times[$article_ref] = array(
                        'reference'       => $article_ref,
                        'name'              => $name,
                        'seconds_per_unity' => $seconds_per_unity
                    );
                }
            }            
            else if (str_contains($data, 'Ficha de Fabricación'))
            {
                preg_match('/Orden Fabricación :(.*)[\t]/', $data, $order_ref);
                preg_match('/Referencia [\t](.*)[\r\n]/', $data, $quantity);
                preg_match('/Cantidad\D+(\d+)Referencia [\t]/', $data, $article_ref);
                preg_match('/Fecha  Final[\r\n](.*)[\r\n]/', $data, $total_time_in_minutes);
                $seconds_per_unity = (int) $total_time_in_minutes[1] * 60 / (int) $quantity[1];

                $items_times[$article_ref[1]] = array(
                    'reference'       => $article_ref[1],                    
                    'seconds_per_unity' => $seconds_per_unity
                );

                $manifest->addItem(new DataImportedManifestItem(
                    $article_ref[1],
                    '',
                    (int) $quantity[1],
                    $date_ship,
                    DateTime::createFromFormat('d/m/y H:i:s', date('d/m/y') . ' 00:00:00'),
                    $order_ref[1],
                    $file
                )); 
            }            
            elseif (str_contains($data, 'Listado Orden de '))
            {
                preg_match('/Orden Envasado : (.*)[\r\n]/', $data, $order_ref);                
                $items_raw = preg_split('/[\r\n]/', preg_split('/Cantidad[\r\n]/', $data)[1]);
                // remove the last two elements (are resumes)
                array_splice($items_raw, -2, 2);

                foreach($items_raw as $item)
                {
                    // reference | name | quantity
                    $info = preg_split('/[\t]/', $item);                                        

                    $manifest->addItem(new DataImportedManifestItem(
                        $info[0],
                        $info[1],
                        (int) $info[2],
                        $date_ship,
                        DateTime::createFromFormat('d/m/y H:i:s', date('d/m/y') . ' 00:00:00'),
                        $order_ref[1],
                        $file
                    )); 
                }
            }
        }        

        foreach($items_times as $reference => $item)
        {
            
            WsArticle::updateOrCreate(
                [
                    'reference'   => $reference,
                    'customer_id' => $this->customer->id
                ],
                array_merge($item, array(
                    'customer_id' => $this->customer->id,
                    'type'        => 'ARTICULO',
                    'active'      => true
                ))
            );
        }        

        return $manifest;
    }

    public function getDateShip(){        
        $hoy = Carbon::now();
        $diaSemana = $hoy->dayOfWeek; 
        // Carbon maneja los días de la semana así:
        // 0 = Domingo, 1 = Lunes, 2 = Martes, 3 = Miércoles, 4 = Jueves, 5 = Viernes, 6 = Sábado

        if (in_array($diaSemana, [1, 2]))
            $fechaEntrega = $hoy->next(Carbon::WEDNESDAY);
        else
            $fechaEntrega = $hoy->next(Carbon::MONDAY);

        $dateToSend = new \DateTime($fechaEntrega->format('Y-m-d H:i:s')); 
        $dateToSend->setTimezone(new \DateTimeZone(config('app.timezone')));

        return $dateToSend;        
    }
    
    static public function validExtensions():array{
        return ['pdf'];
    }

    protected static function getAttachmentOrderDataFilter():callable{
        return function($att){
            return $att->content_type == 'application/pdf' || 
                   $att->content_type == 'application/octet-stream' && Str::endsWith(strtolower($att->name), '.pdf');
        };            
    }

    protected static function getAttachmentOrderFilesFilter():?callable{
        return null;
    }

    protected function filterItemsForPrevisionCondition():callable{
        return function($item){
            return false;
        };
    }
    
    protected function filterItemsForOrderCondition():callable{
        return function($item){
            return true;
        };
    }

    public function middleware(){
        return [new WithoutOverlapping('FKT')];
    }
}
