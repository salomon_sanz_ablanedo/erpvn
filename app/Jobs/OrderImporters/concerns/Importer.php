<?php 

namespace App\Jobs\OrderImporters\concerns;

use App\Jobs\OrderImporters\concerns\DataImportedManifest;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Jobs\OrderImporters\concerns\ReportResume;

use App\Models\WsArticle;
use App\Models\WsOrder;
use App\Models\WsOrderPrevision;
use App\Models\WsOrderLine;

abstract class Importer extends ConsoleOutput
{    
    protected $customer;
    protected DataImportedManifest $manifest;
    protected $mappedReferences;
    public ReportResume $result; // new_articles, orderlines, orders, previsions   
    
    public function __construct($customer, $options){
        $this->customer = $customer;
        parent::__construct();
    }

    abstract public function importNewOrders():DataImportedManifest|false;

    abstract protected function filterItemsForOrderCondition():callable;
    abstract protected function filterItemsForPrevisionCondition():callable; 

    public function import():ReportResume|false
    {        
        $result = $this->importNewOrders();        

        if (!$result) return false;

        $this->manifest = $result;

        $this->result = new ReportResume();

        // filter items without quantity
        $this->manifest->items = array_filter($this->manifest->items, function($item){
            return $item->quantity > 0;
        });

        DB::transaction(function(){
            $this->generateNewArticles();
            $this->activeUsedArticles();
            $this->roundItemsQuantities();
            $this->generatePrevisions();
            $this->generateOrders();
            $this->setLastApplicableOrderDate();
        }, 5);

        return $this->result;
    }

    public function roundItemsQuantities()
    {
        $references = array_keys($this->getDistinctReferences());

        $articles = WsArticle::select(['id','reference','quantity_by_box'])
            ->withoutGlobalScopes()
            ->whereIn('reference', $references)
            ->where('customer_id', $this->manifest->customerId)
            ->where('round_orders_to_box', true)
            ->whereNotNull('quantity_by_box')
            ->get();
        
            //->keyBy('reference')
        //dd($this->manifest->items);
        foreach($articles as $art)
        {
            foreach($this->manifest->items as $key => &$item)
            {
                if ($item->reference == $art->reference && $item->quantity % $art->quantity_by_box != 0)
                {                    
                    //dd($item->quantity);
                    //dd($item->quantity + $art->quantity_by_box - ($item->quantity % $art->quantity_by_box));                    
                    $item->quantity_orig = $item->quantity;
                    $item->quantity = $item->quantity + $art->quantity_by_box - ($item->quantity % $art->quantity_by_box);
                }
            }
        }        
    }

    public function getDistinctReferences(){
        $references = collect($this->manifest->items)->reduce(function($carry, $item){
            if (!array_key_exists($item->reference, $carry))
                $carry[$item->reference] = $item->name;
            return $carry;
        }, []);

        return $references;
    }

    public function activeUsedArticles()
    {
        $references = $this->getDistinctReferences();
        
        WsArticle::whereIn('reference', array_map(function($reference){ return (string) $reference; }, array_keys($references)))
            ->withoutGlobalScopes()
            ->where('customer_id', $this->manifest->customerId)
            ->update(['active' => true]);
    }

    public function generateNewArticles()
    {
        // get distinct references
        $references = $this->getDistinctReferences();        

        $articles = WsArticle::select(['id','reference'])
            ->withoutGlobalScopes() // to get also the inactive articles
            ->whereIn('reference', array_keys($references))
            ->where('customer_id', $this->manifest->customerId)
            ->get()
            ->keyBy('reference')
            ->toArray();           
        
        $newArticles = array_filter($references, function($value, $key) use($articles){            
            return !isset($articles[$key]);
        }, ARRAY_FILTER_USE_BOTH);                

        if (count($newArticles) > 0)
        {
            WsArticle::insert(array_map(function($reference, $name){
                return [
                    'name'        => $name ?? $reference,
                    'reference'   => $reference,
                    'customer_id' => $this->manifest->customerId,
                    'type'        => 'ARTICULO',
                    'active'      => true
                ];
            }, array_keys($newArticles), array_values($newArticles)));            
        }

        $this->result->addNewArticles(count($newArticles));
        
        if (app()->runningInConsole())
            echo 'Created '.count($newArticles) . ' new articles'.PHP_EOL;

        $this->mappedReferences = WsArticle::select(['id','reference'])
            ->withoutGlobalScopes() // to get also the inactive articles            
            ->whereIn('reference', array_keys($references))
            ->where('customer_id', $this->manifest->customerId)
            ->get()
            ->reduce(function($carry, $item){
                $carry[$item->reference] = $item->id;
                return $carry;
            }, []);                    
    }

    public function generatePrevisions()
    {
        $previsions = array_values(collect($this->manifest->items)->filter($this->filterItemsForPrevisionCondition())->toArray());

        if (!empty($previsions))
        {
            // delete previous previsions before insert
            WsOrderPrevision::where('customer_id', $this->manifest->customerId)
                            ->whereNull('user_id')
                            ->delete();

            $previsionsToInsert = array_map(function($prevision){
                return [
                    'customer_id' => $this->manifest->customerId,
                    'article_id'  => $this->mappedReferences[$prevision->reference], // TODO: $prevision,
                    //'article_name'      => $prevision['Description 1'],
                    //'article_reference' => $prevision['Item Number'],
                    'quantity'     => $prevision->quantity,
                    'date_ship'    => Carbon::instance($prevision->date_ship)->format('Y-m-d'),
                    'obs'          => $prevision->obs ?? null
                ];
            }, $previsions);        
    
            WsOrderPrevision::insert($previsionsToInsert);        
        }

        $this->result->addPrevisions(count($previsions));

        if (app()->runningInConsole())        
            echo 'Generated '.count($previsions) . ' previsions'.PHP_EOL;
    }

    public function generateOrders()
    {
        // bug array_filter 16/04/24, ¿why?
        // $orderItems = array_values(collect($this->manifest->items)->filter($this->filterItemsForOrderCondition())->toArray());
        $orderItems = [];

        foreach($this->manifest->items as $item)
        {
            if ($this->filterItemsForOrderCondition()($item))
                $orderItems[] = $item;
        }

        if (empty($orderItems))
        {
            if (app()->runningInConsole())
                echo 'No orders created';
            return;
        }

        $groupedOrderItems = [];

        foreach($orderItems as $i)        
            $groupedOrderItems[$i->group ?? 'default'][] = $i;        

        foreach($groupedOrderItems as $group => $items)
        {
            $neworder = WsOrder::create([
                'date'        => Carbon::instance($items[0]->date_ship)->format('Y-m-d'),
                'customer_id' => $this->manifest->customerId
            ]);
    
            $neworder->save();

            $linesToInsert = array_map(function($item) use($neworder){
                return [
                    'order_id'      => $neworder->id,
                    'article_id'    => $this->mappedReferences[$item->reference],
                    'quantity'      => $item->quantity,
                    'quantity_orig' => $item->quantity_orig,
                    'order_ref'     => $item->order_ref,
                    'obs'           => $item->obs,
                    'prioritary'    => $item->prioritary
                ];
            }, $items);
    
            WsOrderLine::insert($linesToInsert);  

            $this->result->addOrders(1);
            $this->result->addOrderLines(count($linesToInsert));            

            foreach($this->manifest->files as $file)
            {
                $neworder->addMedia($file)
                    //->withCustomProperties(['mime-type' => 'image/jpeg']) //middle method
                    ->preservingOriginal() //middle method
                    ->toMediaCollection($neworder::ORDERFILES_COLLECTION);
            }                              

            if (app()->runningInConsole())
                echo 'Order created with '.count($linesToInsert) . ' items with date: '.$neworder->date.PHP_EOL;
        }
    }

   /*  public function extractArticles()
    {
        $articles = WsArticle::select(['id','reference'])
                  ->whereIn('reference', array_keys($references))
                  ->where('customer_id', $manifest->customerId)
                  ->get()                  
                  ->toArray();

        return Arr::keyBy($articles, 'reference'); 
    } */

    public function setLastApplicableOrderDate(){
        $this->customer->date_last_applicable_order_date = date('Y-m-d');
        $this->customer->save();
    }
}

?>