<?php

namespace App\Jobs\OrderImporters\concerns;

use App\Jobs\OrderImporters\concerns\DataImportedManifestItem;
use Illuminate\Support\Arr;


class DataImportedManifest
{
    public $customerId;
    /** @var DataImportedManifestItem[] */
    public $items = [];    
    public $files = [];

    public function __construct($customerId, $items = [], $files = [])
    {
        $this->customerId = $customerId;                
        $this->files      = Arr::wrap($files);
        $this->items      = $items;
    }

    public function addItem(DataImportedManifestItem $item){
        $this->items[] = $item;
    }    
}