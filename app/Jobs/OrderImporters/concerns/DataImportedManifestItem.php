<?php

namespace App\Jobs\OrderImporters\concerns;

class DataImportedManifestItem
{
    public $reference;
    public $name;
    public $quantity;    
    public $date_order;
    public $date_ship;
    public $order_ref;
    public $file;
    public $obs;
    public $prioritary;
    public $quantity_orig;
    public $group;

    public function __construct($reference, $name, int $quantity, $date_ship, $date_order, $order_ref = null, $file = null, $obs = null, $prioritary = false, $quantity_orig = null, $group = null)
    {
        $this->reference     = $reference;
        $this->name          = $name;
        $this->quantity      = $quantity;
        $this->date_ship     = $date_ship;
        $this->date_order    = $date_order;
        $this->order_ref     = $order_ref;
        $this->file          = $file;
        $this->obs           = $obs;
        $this->prioritary    = $prioritary;
        $this->quantity_orig = $quantity_orig;
        $this->group         = $group;
    }      
}