<?php

namespace App\Jobs\OrderImporters\concerns;

class ReportResume
{
    public $orders       = 0;
    public $ignored      = 0;
    public $orderlines   = 0;
    public $previsions   = 0;
    public $new_articles = 0;
    
    public function addOrders($total){
        $this->orders += $total;
    }

    public function addIgnored(){
        $this->ignored++;
    }

    public function addOrderLines($total){
        $this->orderlines += $total;
    }

    public function addPrevisions($total){
        $this->previsions += $total;
    }

    public function addNewArticles($total){
        $this->new_articles += $total;
    }

    public function addResume(ReportResume $resume)
    {
        $this->addOrders($resume->orders);
        $this->addOrderLines($resume->orderlines);
        $this->addPrevisions($resume->previsions);
        $this->addNewArticles($resume->new_articles);
    }

    public function __toString(): string{
         // new_articles, orderlines, orders, previsions
        return 'Orders found: '     . $this->orders .
        PHP_EOL . 'Orderlines found: ' . $this->orderlines . 
        PHP_EOL . 'Previsions found: ' . $this->previsions .
        PHP_EOL . 'New articles: '     . $this->new_articles.
        PHP_EOL . 'Ignored docs: '     . $this->ignored;
    }

    public function toArray(){
        return [
            'orders'       => $this->orders,
            'orderlines'   => $this->orderlines,
            'previsions'   => $this->previsions,
            'new_articles' => $this->new_articles,
            'ignored'      => $this->ignored
        ];
    }
}