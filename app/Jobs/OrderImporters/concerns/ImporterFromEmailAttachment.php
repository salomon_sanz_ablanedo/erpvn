<?php 

namespace App\Jobs\OrderImporters\concerns;

use App\Jobs\OrderImporters\concerns\ImporterFromFile;
use App\Services\ImapService;
use Webklex\PHPIMAP\Message;

abstract class ImporterFromEmailAttachment extends ImporterFromFile
{
    protected $emailFrom;

    protected $message;

    abstract protected static function getAttachmentOrderDataFilter():?callable;
    abstract protected static function getAttachmentOrderFilesFilter():?callable;

    // override
    public function resolveFiles():array|false
    {
        $this->writeln('============================');
        $this->writeln('Checking orders from: ' . class_basename($this) . ' ('.$this->customer->slug.')');

        if (!empty($this->files)) return $this->files;

        if (!$this->findUnreadEmails())
            return false;        
        
        $result = $this->saveDataAttachment();        

        //$result[] = $this->saveFilesAttachment();        

        return $result;
    }

    public function __construct($customerId, $options){
        parent::__construct($customerId, $options);
        $this->emailFrom = $options['emailFrom'];
    }

    protected function findUnreadEmails():bool
    {
        $messages = ImapService::getUnreadMessages($this->emailFrom, function($m){
            return $m->hasAttachments() && !empty(ImapService::getAttachments($m, static::getAttachmentOrderDataFilter()));
        });

        $this->writeln(' Email messages found: '.count($messages));
        
        // TODO: get the newest

        if (empty($messages)) 
            return false;

        $this->message = $messages[0];
        
        return true;
    }

    protected function getAttachmentLocalPath($att){
        return storage_path('app/orderimports/'.$this->customer->slug.'-'.date('d-m-y').'.'.$att->name);
    }

    protected function saveAttachment($att)
    {        
        $file = $this->getAttachmentLocalPath($att);
        $dir = dirname($file);

        if (!file_exists($dir))
            mkdir($dir);

        $att->save($dir.'/', basename($file));
        return true;
    }    

    public function onImported(){        
        //foreach($messages as $m)
        ImapService::markAsRead($this->message);
    }

    private function saveDataAttachment()
    {
        $attOrder = ImapService::getAttachments($this->message, static::getAttachmentOrderDataFilter());            
        $result = [];

        foreach($attOrder as $att)
        {
            $this->saveAttachment($att);
            $result[]  = $this->getAttachmentLocalPath($att);
        }

        return $result;
    }

    private function saveFilesAttachment()
    {
        $att = ImapService::getAttachments($this->message, static::getAttachmentOrderFilesFilter());
        
        $files = [];

        foreach($att as $a)
        {
            $files[] = $this->getAttachmentLocalPath($a);
            $this->saveAttachment($a);
        }

        return $files;
    }
}

?>