<?php 

namespace App\Jobs\OrderImporters\concerns;

use App\Jobs\OrderImporters\concerns\DataImportedManifest;
use App\Jobs\OrderImporters\concerns\Importer;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Exception;

abstract class ImporterFromFile extends Importer
{    
    protected $files = [];

    abstract public function importFromFile($file):DataImportedManifest|false;
    abstract static public function validExtensions():array;
    abstract public function isFileProcessable($file):bool;
    abstract public function isFileIgnorable($file):bool;

    public function isValidExtension($file):bool{
        $extensions = Arr::map(static::validExtensions(), function ($value, $key){
            return '.'.strtolower($value);
        });

        return Str::endsWith(strtolower($file), $extensions);
    }
    
    public function isValidFile($file):bool{
        return $this->isValidExtension($file) && $this->isFileProcessable($file);
    }

    public function setFiles($files){
        $this->files = Arr::wrap($files);
    }

    public function resolveFiles():array|false{
        return $this->files;
    }

    public function importNewOrders():DataImportedManifest|false{

        $files = $this->resolveFiles();      
        
        // TODO: resolve extrafiles                

        if (empty($files)) return false;        

        $manifest_items = [];
        $manifest_files = [];                  

        // merge all manifest in one manifest       
        foreach($files as $file)
        {            
            if ($this->isFileIgnorable($file))
                continue;
         
            if (!$this->isValidExtension($file))
                throw new Exception('Extension of the file is not valid: '.$file);

            if (!$this->isFileProcessable($file))
                throw new Exception('File is not processable, doesnt contains the expected data format');

            $m = $this->importFromFile($file);               
            $manifest_items = [...$manifest_items, ...$m->items];
            $manifest_files = [...$manifest_files, ...$m->files];    
        }                

        return new DataImportedManifest($this->customer->id, $manifest_items, $manifest_files);
    }    
}

?>