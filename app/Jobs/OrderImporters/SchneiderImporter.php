<?php

namespace App\Jobs\OrderImporters;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Carbon\Carbon;
use App\Models\WsOrderPrevision;
use App\Models\WsArticle;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use Illuminate\Support\Arr;
use App\SimpleBrowser;

class SchneiderImporter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct($customerId){

    }

    //php artisan job:dispatch OrderImport --param="customerId=2"
    public function handle()
    {
        $this->writeln('============================');
        $this->writeln('Checking orders from: ' . class_basename($this));

        $browser = new SimpleBrowser('https://wanapix.es');//('https://wanapix.es');
        $browser->browse(function($b){
            $b->visit('https://wanapix.es')
              ->visit('regalos-navidad-personalizados')
              ->screenshot('filename');
        });
    }     
    
    public function middleware(){
        return [new WithoutOverlapping('SCHN')];
    }
}
