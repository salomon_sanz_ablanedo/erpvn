<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\WsOrderPrevision;
use App\Models\WsOrder;
use App\Models\WsOrderLine;
use Illuminate\Support\Arr;
use Webklex\IMAP\Facades\Client;
use App\Models\WsCustomer;
use Exception;
//use App\Services\OrderImportService;
use App\Jobs\OrderImporters\concerns\ReportResume;
use Throwable;


//php artisan job:dispatch OrderImport --param="customerId=1"
class OrderImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;    

    private $params;
    //public $result;

    public function __construct($params = []){        
        $this->params = $params;
    }

    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
        //echo 'asdffsdasdfasdfasdfasdf';
        throw $exception;
    }

    public function handle()
    {        
        $result = new ReportResume();        

        $customers = WsCustomer::whereNotNull('order_importer_id');

        if (isset($this->params['customerId']))
            $customers->where('id',$this->params['customerId']);

        $customers = $customers->get();       
        $errors = [];

        foreach($customers as $customer)
        {
            $params = $customer->order_importer_params ? json_decode($customer->order_importer_params, true) ?? [] : [];            
            
            $params = array_merge($params, $this->params ?? []);

            $class = '\\App\\Jobs\OrderImporters\\'.$customer->orderImporter->job_class;

            $importer = new $class($customer, $params ?? null);

            try
            {
                $timestamp = $customer->freshTimestamp(); 

                $r = $importer->import();

                if (!empty($r))
                {
                    $result->addResume($r);
                    // new_articles, orderlines, orders, previsions
                    $importer->onImported();
                    $customer->date_last_import_found = $timestamp;
                    $customer->date_last_import_success = $timestamp;
                    // touch date_last_import_success
                    // touch date_last_import_found
                }
                else
                {
                    $customer->date_last_import_success = $timestamp;
                }

                $customer->save();
            }
            catch(Exception $e)
            {
                $errors[] = $e;
                $customer->date_last_import_error = $timestamp;
                $customer->save();
            }
        }

        if (app()->runningInConsole())
            echo $result;

        if (!empty($errors))                    
            $this->fail($errors[0]);        

        return $result;
    }
}
