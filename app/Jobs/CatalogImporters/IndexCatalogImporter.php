<?php

namespace App\Jobs\CatalogImporters;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Models\WsArticle;
use App\Models\WsOrderLine;
use App\Models\WsOrderPrevision;
use App\Models\WsStockMovement;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Exception;
use Throwable;


//php artisan job:dispatch OrderImport --param="customerId=1"
class IndexCatalogImporter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;    

    private $params;

    public function __construct($params = []){        
        $this->params = $params;
    }

    public function failed(Throwable $exception)
    {

    }

    public function handle()
    {        
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $result = [
            'items_created' => 0,
            'subitems_created' => 0,
            'subitems_linked' => 0,            
        ];        

        $fields = [
            'component_id', // 0 = pending, 1 = completed, 2 = require update
            'article_id',
            'quantity',
            'fam_material',
            'fam_stats',
            'fam',
            'size'
        ];        

        $list_main = [];
        $list_subitems = [];
        $list_conflicts = [];
        $list_parent_refs_not_found = [];

        if (($gestor = fopen($this->params['file'], "r")) == FALSE) return;        

        // collect csv data creating list of articles and components
        while (($datos = fgetcsv($gestor, 70000, ',')) !== FALSE)
        {            
            $row = array_combine($fields, $datos);

            if (in_array( $row['component_id'], ['-','COMPONENTE']))
                continue;

            $row['quantity'] = (float) str_replace(',', '.', $row['quantity']);            

            if ($row['size'] == '-')
                $row['size'] = '';

            if (!isset($list_main[$row['article_id']]))
                $list_main[$row['article_id']] = [];

            $list_main[$row['article_id']]['COMPONENTS'][$row['component_id']] = array(
                'quantity' => $row['quantity'],
                'active' => false,
            );

            if (!isset($list_subitems[$row['component_id']]))
                $list_subitems[$row['component_id']] = Arr::except($row, ['component_id','article_id','quantity']);                           
        }        

        fclose($gestor);        


        // create the non existing articles
        foreach($list_main as $reference => $item)
        {
            $art = WsArticle::where('customer_id', 3)->where('reference', $reference)->first();

/*             if ($reference == 'MAE1442')
                Log::debug($art); */

            if (!empty($art))
            {
                $list_main[$reference]['id'] = $art->id;
                $list_main[$reference]['active'] = $art->active;
                continue;
            }

            //$seconds_per_unity = $this->getSecondsPerUnity($item);
            //Log::info('Creating article: '.$reference);

            try{
                $id = WsArticle::insertGetId([
                    'name'        => '',
                    'reference'   => $reference,                
                    'customer_id' => 3,
                    'type'        => 'ARTICULO',
                    'active'      => 0
                    //'stock',
                    //$stock_min,
                    //'seconds_per_unity' => $seconds_per_unity,                
                ]);   

                $list_main[$reference]['id'] = $id;
                $list_main[$reference]['active'] = false;
            }
            catch(Exception $e)
            {                
                Log::info('Error creating item: '.$reference);
            }
            
            $result['items_created']++;
        }
        
 /*        Log::info($list_main['SI08050WAZ']);
        return; */
        
        // create the non existing subarticles
        foreach($list_subitems as $component_reference=>&$subitem)
        {            
            $art = WsArticle::where('customer_id', 3)->where('reference', $component_reference)->first();
            
            if (!empty($art))
            {
                //dd($art->id);
                $list_subitems[$component_reference]['id'] = $art->id;
                $list_subitems[$component_reference]['active'] = $art->active;
                continue;
            }

            try{
                $list_subitems[$component_reference]['id'] = WsArticle::create([
                    'name'        => trim((explode(' - ', $subitem['fam_material'])[1] ?? ''). " ". $subitem['size']),
                    'reference'   => $component_reference,                
                    'customer_id' => 3,                
                    'type'        => 'COMPONENTE',
                    'active'      => 0      
                ])->id;

                $list_subitems[$component_reference]['active'] = 0;

            }catch(Exception $e)
            {                
                Log::info('Error creating subitem: '.$component_reference);
            }

            $result['subitems_created']++;
        }            
        
        // $list_main[$REFERENCE_ID][$SUB_ITEM_REFERENCE] = 1;        

        // link the components to the articles
        foreach($list_main as $reference =>&$article)
        {                        
            foreach($article['COMPONENTS'] as $component_reference => &$component)
            {
                try
                {
                    if ($component_reference == 'id') continue;

                    //echo '&nbsp;&nbsp;&nbsp;&nbsp;Component: '. $component_id. '<br>';
                    try
                    {                    
                        // if can be a article or a component
                        if (isset($list_subitems[$component_reference]))
                            $component['id'] = $list_subitems[$component_reference]['id'];
                        else
                            $component['id'] = $list_main[$component_reference]['id'];

                        if (DB::table('ws_article_items')->where('article_id', $article['id'])
                                                         ->where('article_item_id', $component['id'])
                                                        ->get()->isEmpty())
                        {
                            DB::table('ws_article_items')->insert([
                                'article_id'      => $article['id'],
                                'article_item_id' => $component['id'],
                                'quantity'        => $component['quantity']                                
                            ]);
            
                            $result['subitems_linked']++;
                        }
                    }
                    catch(Exception $e)
                    {
                        $list_parent_refs_not_found[] = $component_reference;
                        continue;
                    }

                    // update the article seconds per unity
                    $wsarticle = WsArticle::with('items')->find($article['id']);

                    if (empty($wsarticle))
                    {
                        //Log::info('Article not found: '.$article['id']);
                        continue;
                    }

                    if (!empty($wsarticle->seconds_per_unity))
                        continue;
                    
                    
                    $wsarticle->seconds_per_unity = $wsarticle->items->reduce(function ($carry, $sitem, $subitemkey) use($list_subitems, $item) {
                        $subitem = $list_subitems[$sitem['reference']];                     
                        $seconds = $this->getSecondsPerUnity($sitem['reference'], $subitem, $item[$sitem['reference']]);
                        //echo $sitem['reference'] . ' = '.$seconds.'<br/>';
                        return $carry + $seconds;
                    }, 0);

                    $total_orderlines = WsOrderLine::where('article_id', $article['id'])->count();
                    $total_stock_movements = 0;
                    $total_previsions = 0;

                    if ($total_orderlines == 0)
                    {
                        $total_stock_movements = WsStockMovement::where('article_id', $article['id'])->count();

                        if ($total_stock_movements == 0)                
                            $total_previsions = WsOrderPrevision::where('article_id', $article['id'])->count();                
                    }
                    
                    if ($total_orderlines > 0 || $total_stock_movements > 0 || $total_previsions == 0)
                    {
                        $wsarticle->active = 1;
                        $wsarticle->items->each(function($item){
                            $item->active = 1;
                            $item->save();
                        });
                    }
                    else
                        $wsarticle->active = 0;            

                    $wsarticle->save();   
                }
                catch(Exception $e)
                {
                    $list_conflicts[] = $component_reference;
                }
            }         
        }

        Log::info('List of conflicts - x'.count($list_parent_refs_not_found).' parents ref not found: '.implode(', ', $list_parent_refs_not_found));
        Log::info('List of conflicts - x'.count($list_conflicts).' are article and component: '.implode(', ', $list_conflicts));
    }

    public function getSecondsPerUnity($reference, $item, $quantity):float
    {        
        $box_table = [
            'BO'=>1,
            'ET'=>0,
            'P'=>0,
            'SELLO'=>0,
            'ZXCAIN001'=>6,
            'ZXCAIN002'=>6,
            'ZXCAIN003'=>7,
            'ZXCAIN004'=>7,
            'ZXCAIN005'=>9,
            'ZXCAIN312'=>8,
            'ZXCAINL00'=>7,
            'ZXCAINL01'=>7,
            'ZXCAINL02'=>7,
            'ZXCAINL03'=>7,
            'ZXCALS01'=>8,
            'ZXCALS02'=>8,
            'ZXCAIA002'=>10,
            'ZXW037C'=>10,
            'ZXETIA07555'=>10,
            'ZXCEBIN01'=>10,
            'ZXCEBIN02'=>11,
            'ZXCAMA002'=>11,
            'ZXCEBMA02'=>11,
            'ZXCEBMA01'=>11,
            'ZXCEBMA02'=>11,
            'ZXCAVEN06'=>4,
            'ZXTEX004' =>3            
        ];

        // is box similar
        if ($quantity < 1)
        {
            $default = 8;
            $result = array_reduce(array_keys($box_table), function($carry_item, $key) use($reference, $quantity, $box_table){
                if (Str::startsWith($reference, $key))
                    $carry_item = $box_table[$key] * $quantity;
                return $carry_item;
            }, $default);
        }
        else
        {
            if ($item['fam'] == '11 - ARANDELAS')
            {
                if (Str::contains($item['fam_material'], 'E.P.D.M'))
                    $result = $quantity * 0.6;
                else
                    $result = $quantity * 2.3;
            }
            else if ($item['fam'] == '66 - TUERCAS')
                $result = $quantity * 3;
            else if (Str::contains($item['fam_material'], 'CONO'))
                $result = $quantity * 1.5;
            else if (Str::contains($item['fam_material'], 'CAMISA'))
                $result = $quantity * 0.6;
            else if (Str::contains($item['fam_stats'], 'ARANDELA'))
                $result = $quantity * 0.6;
            else
                $result = $quantity * 0.5;
        }

        return $result;
        //(float) str_replace(',', '.', $item['time']); // 3,2 ==> 3.2
    }
}
