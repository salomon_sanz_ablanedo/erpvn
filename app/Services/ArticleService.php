<?php

namespace App\Services;

use App\Models\WsStockMovement;
use App\Models\WsArticle;
use Auth;
use Log;

class ArticleService
{
    public static function getOrderTime($article, $quantity, $rounded = true){
        
        $hours = ($article->seconds_per_unity * $quantity) / 3600;

        if ($rounded)
            $hours = static::roundTo5($hours);

        return $hours;
    }

    public static function getOrderTime4Humans($hours){   
        $people = str_replace(',0','', number_format(round($hours / 7.5, 1), 1, ','));
       /*  if ($people < 1)
            $people = 1; */
        return str_replace(',0','', number_format($hours, 1, ',')) . 'h. ('.$people.' pers)';
    }

    public static function getOrderTime4HumansByArticle($article, $quantity, $rounded = true){
        $hours = static::getOrderTime($article, $quantity, $rounded);       
        return static::getOrderTime4Humans($hours);       
    }

    public static function roundTo5($number){
        return round($number * 2) / 2;
    }
}