<?php

namespace App\Services;

use App\Models\WsOrder;
use App\Models\WsOrderLine;
use App\Services\_SingletonService;
use Illuminate\Support\Facades\DB;
use Log;
use Illuminate\Support\Arr;
use Carbon\Carbon;


class OrderService extends _SingletonService
{
    public function updateDeliveredUnits(WsOrder $order)
    {
        //Log::info('OrderService->updateDeliveredUnits of order:'.$order->id);
        //$notes = DeliveryNote::select('id')->ofOrder($order->id)->get();
        //$lines = DeliveryNoteLine::select('article_id', DB::raw('SUM(quantity) as cantidad'))->whereIn('delivery_id', $notes)->groupBy('article_id')->get();

        $delivery_lines = DB::table('delivery_note_lines')
            ->join('delivery_notes', 'delivery_note_lines.delivery_id', '=', 'delivery_notes.id')
            ->join('ws_orders', 'delivery_notes.order_id', '=', 'ws_orders.id')            
            ->select('delivery_note_lines.article_id', DB::raw('SUM(quantity) as cantidad'), 'delivery_note_lines.obs','delivery_note_lines.order_ref', 'delivery_note_lines.prioritary')
            ->groupByRaw('delivery_note_lines.article_id, delivery_note_lines.obs, delivery_note_lines.order_ref, delivery_note_lines.prioritary')
            ->where('ws_orders.id', $order->id)
            ->get();

        //Log::info('Total lines found to recalc: '.$delivery_lines->count());

        $supplied = [];

        $delivery_lines->each(function($l) use(&$supplied){
            $supplied[] = [
                'article_id' => $l->article_id,
                'quantity' => $l->cantidad,
                'obs'      => $l->obs,
                'order_ref' => $l->order_ref,
                'prioritary' => $l->prioritary
            ];
        });

        $order->lines->each(function($orderline) use(&$supplied)
        {
            $found = Arr::first($supplied, function($value, $key) use(&$orderline){
                $result = ((!empty($value['obs']) && $value['obs'] == $orderline->obs) || (!empty($value['order_ref']) && $value['order_ref'] == $orderline->order_ref)) && $value['article_id'] == $orderline->article_id && $value['prioritary'] == $orderline->prioritary;
                $orderline['found'] = true;
                return $result;
            });

            if (empty($found))
            {                
                $supplied[] = [
                    'article_id' => $orderline->article_id,
                    'quantity'   => 0,
                    'obs'        => $orderline->obs,
                    'order_ref'  => $orderline->order_ref,
                    'prioritary' => $orderline->prioritary
                ];
            }
        });

        //Log::debug($supplied);        

        foreach($supplied as $supplied_item)
        {            
            $line = WsOrderLine::where([
                ['order_id', $order->id],
                ['article_id', $supplied_item['article_id']],
                ['prioritary', $supplied_item['prioritary']]                
            ])->when(!empty($supplied_item['obs']) || !empty($supplied_item['order_ref']), function($q) use($supplied_item){
                return $q->whereRaw('((obs IS NOT NULL AND obs="'.$supplied_item['obs'].'") OR (order_ref IS NOT NULL and order_ref="'.$supplied_item['order_ref'].'"))');
            })
            ->first();                                        

            //if (!empty($line))
            //    Log::info('lineid: '.$line->id . ' | $supplied_item[quantity] = '.$supplied_item['quantity'].' | line->quantity: = '. $line->quantity);

            if (!empty($line) && $supplied_item['quantity'] <= $line->quantity)
                $line->updateQuietly(['quantity_supplied'=>$supplied_item['quantity']]);
        }

        $this->updateOrderStatus($order);

        //Log::info('Article '.$l->article_id . '(old/new) '.$oldsupplied .'/'.$orderline->quantity_supplied);                  
        //Log::info(vsprintf(str_replace(['?'], ['\'%s\''], $query->toSql()), $query->getBindings()));                              
    }

    public function updateOrderStatus(WsOrder $order)
    {
        // WHY COUNT doesnt work??? 19/04/24
        $lines = WsOrderLine::where('order_id', $order->id)->get();
        $total_lines = $lines->count();

        $total_completed = $lines->reduce(function (?int $carry, $item) {
            if ($item->status == 'CANCELADO' || $item->quantity == $item->quantity_supplied)
                $carry++;
            return $carry;
        }, 0);

        //Log::debug('Total lines: '. $total_lines . ' | Total completed: ' . $total_completed);                

        if ($total_lines == $total_completed)
        {
            $order->status= 'COMPLETED';    
            $order->date_completed = Carbon::now();            
        }
        else
        {
            $order->status= 'PENDING';    
            $order->date_completed = null;
        }
                
        $order->save();
    }

    public function updateOrderProductionTimings(){

    }

    public function updateTotalOrdersProductionTimings(){
        
    }
}