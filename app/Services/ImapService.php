<?php

namespace App\Services;

use App\Models\WsStockMovement;
use App\Models\WsArticle;
use Auth;
use Webklex\IMAP\Facades\Client;
use Illuminate\Support\Arr;

class ImapService
{   
    static protected $client;

    static public function connect()
    {
        if (isset(static::$client))
            return;

        static::$client = Client::account('gmail');
        static::$client->connect();
    }

    static public function getUnreadMessages($from, callable $filter = null)
    {                
        static::connect();

        # https://www.php-imap.com/api/query
        $messages = static::$client->getFolder('INBOX')->messages()->from($from)->unseen()->get();                

        if (empty($messages)) 
            return;        
       /* else
            $messages = $messages->all(); */                    
        
        /* foreach($att as $a)
            echo $a->name .'=>'.$a->content_type;
        exit; */
        
        if (!empty($filter))
            $messages = Arr::where($messages->toArray(), $filter);        

        return array_values($messages);   // to get ordered 0 => ....
    }

    /* static public function hasAttachmentsFilter($type = null){
        return function($m) use($type){                        
            return $m->hasAttachments() && count(static::getAttachments($m, $type)) > 0;
        };
    } */

    static public function getAttachments($message, callable $filter = null)
    {        
        $messages = Arr::where($message->getAttachments()->toArray(), $filter ?? fn()=>true);        
        
        return array_values($messages);  
        /*
        because returns keyed attachments:
        'asfef3f3' => attachment, etc
        */
    }

    static public function markAsRead($message){
        // TODO
        //https://www.php-imap.com/api/query
        //dd($client->getFolder('INBOX')->messages()->);
        //
        static::$client->getFolder('INBOX')->query()->markAsRead()->getMessage($message->getUid());
        /* $oFolder->query()->text('Hello world')->markAsRead()->get();
        $message->markAsRead(); */
    }
}