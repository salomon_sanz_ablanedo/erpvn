<?php

namespace App\Services;

use App\Models\WsStockMovement;
use App\Models\WsArticle;
use Auth;
use Illuminate\Support\Facades\Log;

class StockService
{
    public static function getStock($article_id){    
        return WsStockMovement::ofArticle($article_id)->sum('quantity');
    }

    public static function updateStock($article_id, $force_stock = null)
    {        
        $stock = static::getStock($article_id);
        $art = WsArticle::find($article_id);

        // avoid negative stock (Marzio request)
        if ($stock < 0 || isset($force_stock) && $force_stock < 0)
            $force_stock = 0;        

        if ($force_stock !== null)
        {            
            // Log::info('StockService: updateStock force_stock '.$force_stock.' in #'.$article_id);
            //if ($force_stock == 0 && $stock == 0) return;
            WsStockMovement::insert([
                'article_id' => $article_id,
                'quantity'   => ($stock * -1) + $force_stock,
                'user_id'    => Auth::user()->id
            ]);

            $art->stock = $force_stock;         
        }
        else
        {
            // Log::info('StockService: updateStock #'.$article_id);
            $art->stock = $stock;
        }

        $art->save();
    }

    public static function addMovement($article_id, $quantity)
    {
        //Log::info('StockService: add movement #'.$article_id);

        WsStockMovement::create(array(
            'article_id' => $article_id,
            'quantity'   => $quantity,
            'user_id'    => Auth::user()?->id ?? null
        ));        
    }

    public static function setComponentsMovements($article, $quantity)
    {
        if (!$article->isTypeArticle()) return;     
        //Log::info('StockService: setComponentsMovements #'.$article->id);

        $article->items->each(function($subitem) use($quantity){ 
            $subitem_quantity = $subitem->pivot->quantity * $quantity;
            // marzio dont want negative stock
            if ($subitem_quantity > 0 || ($subitem_quantity < 0 && $subitem->stock >= abs($subitem_quantity)))
                StockService::addMovement($subitem->id, $subitem_quantity);
        });
    }

}