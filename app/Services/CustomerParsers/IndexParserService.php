<?php

namespace App\Services\CustomerParsers;

use Illuminate\Support\Str;
use App\Jobs\OrderImporters\concerns\DataImportedManifest;

class IndexParserService
{
    private static $instance;

    const PAGE_DELIMITATOR = 'Frecuency';

    private $rawData = [];

    private $items = [];    

	public static function INSTANCE()
    {
     	if ( !self::$instance instanceof self)
        	self::$instance = new self;
	    return self::$instance;
    }

    public function loadFile($file){
        $this->rawData = [];
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($file);    

        foreach($pdf->getPages() as $page)
            $this->rawData[] = $page->getText();
    }

    public function parseFile($file)
    {
        $this->loadFile($file);

        foreach($this->rawData as $data)
        {
            
        }
    }

    public function getHeader($page = 0)
    {
        $data = $this->rawData[$page];

        dd($data);

        //dd(explode('\t\n', $data));
        /* $data = preg_replace('/\t/', '', $data);        
        $lines = preg_split('/\r\n|\r|\n/', $data); */
        $header = [];

        preg_match('/\* \*(.*)\*/', $data, $matches);

        $header['order_ref'] = $matches[1];

        return $header;

        dd($matches);

        foreach($lines as $line)
        {
            if (Str::startsWith($line, 'Prior')) break;
            if (!Str::contains($line, ':')) continue;                        
            [$key, $value] = explode(':', $line);
            $header[trim($key)] = trim($value);
        }

        return $header;        
    }
    /*
    Supplier: \tS0008 \t\n
ASOCIACION CRISTIANA VIDA NUEVA\n
Camino del molino 1 Bajo\n
Ciriza, 31 31174\n
SPAIN \n
Release ID: \t20221223-001 \t\n
Purchase Order: \tPSS0008 \t\n
Item Number: \t0003 220 0009 \t\n
Release Date: \t23/12/22 \t\n
In transit Qty: \t0,0 \t\n
Receipt Date: \t21/12/22 \t\n
Receipt Qty: \t100,0 \t\n
Cum received: \t323.002,0 \t\n
Last Delivery Note: \t1588 \t\n
Date \tDiscret Qty \tAcumulatted Qty \tNet Qty \t\n
Our Precision, Your Advantage \t\n
UPDATE SCHEDULE - KAMS \t\n
Fecha:  23/12/2022 \n
Hora:     4:30:09 \t\n
Pág...1 \t\n
Frecuency \t\n
Dock Code: \tST0000 \t\n
Prior \t323.002,0 \t0,0 \t\n
03/01/23 \t100,0 \t323.102,0 \t100,0 \tDaily \t\n
04/01/23 \t100,0 \t323.202,0 \t100,0 \t\n
10/01/23 \t400,0 \t323.602,0 \t400,0 \t\n
11/01/23 \t100,0 \t323.702,0 \t100,0 \t\n
12/01/23 \t200,0 \t323.902,0 \t200,0 \t\n
13/01/23 \t100,0 \t324.002,0 \t100,0 \t\n
16/01/23 \t200,0 \t324.202,0 \t200,0 \t\n
17/01/23 \t200,0 \t324.402,0 \t200,0 \t\n
18/01/23 \t100,0 \t324.502,0 \t100,0 \t\n
19/01/23 \t200,0 \t324.702,0 \t200,0 \t\n
23/01/23 \t200,0 \t324.902,0 \t200,0 \t\n
24/01/23 \t200,0 \t325.102,0 \t200,0 \t\n
25/01/23 \t200,0 \t325.302,0 \t200,0 \t\n
26/01/23 \t100,0 \t325.402,0 \t100,0 \t\n
30/01/23 \t300,0 \t325.702,0 \t300,0 \t\n
31/01/23 \t300,0 \t326.002,0 \t300,0 \t\n
02/02/23 \t200,0 \t326.202,0 \t200,0 \t\n
03/02/23 \t200,0 \t326.402,0 \t200,0 \t\n
06/02/23 \t200,0 \t326.602,0 \t200,0 \t\n
07/02/23 \t300,0 \t326.902,0 \t300,0 \t\n
08/02/23 \t200,0 \t327.102,0 \t200,0 \t\n
09/02/23 \t200,0 \t327.302,0 \t200,0 \t\n
13/02/23 \t200,0 \t327.502,0 \t200,0 \t\n
14/02/23 \t300,0 \t327.802,0 \t300,0 \t\n
15/02/23 \t100,0 \t327.902,0 \t100,0 \t\n
16/02/23 \t200,0 \t328.102,0 \t200,0 \t\n
17/02/23 \t200,0 \t328.302,0 \t200,0 \t\n
20/02/23 \t200,0 \t328.502,0 \t200,0
*/
}