<?php

namespace App\Services\CustomerParsers;

use Illuminate\Support\Str;
use App\Jobs\OrderImporters\concerns\DataImportedManifest;

class KybseParserService
{
    const PAGE_DELIMITATOR = 'Frecuency';

    private $rawData = [];

    private static $instance;

	public static function INSTANCE()
    {
     	if ( !self::$instance instanceof self)
        	self::$instance = new self;
	    return self::$instance;
    }

    public function getPages($index = null){
        if ($index === null)
            return $this->rawData;
        else
            return $this->rawData[$index];
    }

    public function loadFile($file){
        $this->rawData = [];
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($file);        

        foreach($pdf->getPages() as $page)
            $this->rawData[] = $page->getText();        
    }    

    public function getHeader($page = 0)
    {
        $data = $this->rawData[$page];        
        $data = preg_replace('/\t/', '', $data);        
        $lines = preg_split('/\r\n|\r|\n/', $data);
        $header = [];

        foreach($lines as $line)
        {
            if (Str::startsWith($line, 'Prior')) break;
            if (!Str::contains($line, ':')) continue;                        
            [$key, $value] = explode(':', $line);
            $header[trim($key)] = trim($value);
        }

        return $header;        
    }

    public function getItems($page = 0)
    {
        $data = $this->rawData[$page];        

        $data = preg_replace('/\t/', '|', $data);    
        $data = str_replace('Daily', '', $data);                    
        $data = str_replace('Monthl', '', $data);                

        if (Str::contains($data, 'Prior'))
        {                    
            $data = explode('Frecuency', $data)[1];            
            //$data = explode('Prior', $data)[1];
            
            // kybse is different from kams, has a footer in the PDF that has to be deleted
            $data = explode('Safety Stock', $data)[0];            
            $lines = preg_split('/\r\n|\r|\n/', $data);              
            array_shift($lines); // remove empty line
            array_shift($lines); // remove: Dock Code: etc
        }
        else
        {
            // sometimes a order contain serveral pages
            $lines = preg_split('/\r\n|\r|\n/', explode(static::PAGE_DELIMITATOR, $data)[1]);            
            array_shift($lines); // remove empty line
        }        

        $lines = array_filter($lines); // to clean empty lines (sometimes in kybse)                

        $header = $this->getHeader($page);        

        // when is a multipage order doesnt have header, get the previous header
        if (!isset($header['Item Number']))
            $header = $this->getHeader($page-1);

        // except: ItemNumber, Receipt Qty, Cum received, Last Delivery Note
        $items = [];

        // handle PRIORITARY items
        if (Str::startsWith($lines[0], 'Prior'))
        {
            if (!Str::endsWith($lines[0], '|0,0'))
            {                
                $items[] = array(                    
                    'item'       => $header['Item Number'],
                    'date'       => (string) date('d/m/y'),
                    'quantity'   => Str::afterLast($lines[0], '|'),
                    'prioritary' => true
                );
            }

            // remove prior
            array_shift($lines);
        }
        
                
        foreach($lines as &$line)
        {
            [$date, $quantity] = explode('|', $line);

            $items[] = array(
                'item'       => $header['Item Number'],
                'date'       => $date,
                'quantity'   => $quantity,
                'prioritary' => false,
            );
        }

        return $items;   
    }   
}