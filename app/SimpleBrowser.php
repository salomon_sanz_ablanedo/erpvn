<?php

namespace App;

use Closure;
use Exception;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Laravel\Dusk\Chrome\ChromeProcess;
use Laravel\Dusk\Chrome\SupportsChrome;
use Symfony\Component\Process\Process;
use Throwable;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Concerns\ProvidesBrowser;

class SimpleBrowser
{
    use ProvidesBrowser, SupportsChrome;

    private $baseURL;

    public function __construct($baseURL){
        $this->baseURL = $baseURL;
        static::prepare();
        $this->setUp();
    }

    public function getName(){
        return 'NAME';
    }

    public static function prepare()
    {        
        if (! static::runningInSail()) {
            static::startChromeDriver();
        }
    }

    /**
     * Register the base URL with Dusk.
     *
     * @return void
     */
    protected function setUp(): void
    {
        Browser::$baseUrl = $this->baseUrl();

        Browser::$storeScreenshotsAt = base_path('tests/Browser/screenshots');

        Browser::$storeConsoleLogAt = base_path('tests/Browser/console');

        Browser::$storeSourceAt = base_path('tests/Browser/source');

        Browser::$userResolver = function () {
            return $this->user();
        };
    }

    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            //'--start-maximized',
            '--window-size=1920,1080',                    
            '--disable-gpu',
            //'--headless',            
        ]);       

        // https://stackoverflow.com/questions/50405381/file-download-in-headless-chrome-using-laravel-dusk
        $options->setExperimentalOption('prefs', [
            'download.default_directory' => storage_path('temp')
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515',
            DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }

    protected function baseUrl()
    {
        //return rtrim(config('app.url'), '/');
        return rtrim($this->baseURL, '/');
    }

    protected function user()
    {
        throw new Exception('User resolver has not been set.');
    }

    protected static function runningInSail()
    {
        return isset($_ENV['LARAVEL_SAIL']) && $_ENV['LARAVEL_SAIL'] == '1';
    }

}