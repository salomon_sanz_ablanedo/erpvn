
<html>
	<head>
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <style type="text/css">
            * { 
                -moz-box-sizing: border-box; 
                -webkit-box-sizing: border-box; 
                box-sizing: border-box; 
            }
            html, body{
                margin:0px !important;
                padding:0px !important;
                width: 210mm;
                font-family:Arial;
                font-size: 12pt;
            }
            h1{
                display: inline;
            }
            .page{
                margin:0px;
                width:210mm; 
                height:297mm; 
                padding:5mm 10mm;
                display: inline-block;
                overflow: hidden;
            }

            .small{
                font-size: 8pt;
            }

            table.table{
                border-spacing: 0;
                border-collapse: collapse;
                vertical-align: top;
            }
            
		</style>
	</head>
	<body>
        <div class="page">
            <table style="width:100%">
                <tr>
                    <td style="width:55%; text-align:center;">
                        <img src="{{ asset('images/logo.png') }}" style="width:350px;"/>
                        <div class="small">
                        Asociacion Cristiana Vida Nueva  <br/>
                        Camino del molino, s/n, 31174 Ciriza, Navarra<br> 
                        Tel 948 32 22 01
                        </div>
                    </td>
                    <td style="text-align:center; width:45%;">

                        <table class="table" style="border:1px solid #000; width:100%;" cellpadding="5">
                            <tr>
                                <td colspan="2" style="text-align: center; border-bottom:1px solid #999;"><h4 style="display:inline;">PEDIDO</h4></td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" class="small">
                                    Pedido: <br>                                                                                                                                             
                                </td>
                                <td style="text-align:left;">
                                    {{ $order->reference }}
                {{--                     @if (!empty($note->order->reference))
                                        | <span class="small">Ped. Ref: {{ $note->order->reference }}</span>
                                    @endif --}}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" class="small">
                                    Fecha: <br>
                                </td>
                                <td style="text-align:left;">
                                    {{ date('d/m/Y', strtotime($order->date)) }}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" class="small">
                                    Cliente: <br>
                                </td>
                                <td style="text-align:left;">
                                    {{ $order->customer->name }} <br/>
                                    <span class="small">{{ $order->customer->address }}</span>

                                </td>
                            </tr>
                            {{-- <tr>
                                    Pedido cliente: <br>
                                    Fecha: <br/>
                                    Cliente: <br/>
                                </td>
                                <td>
                                    Pedido nº: <br>
                                    Pedido cliente: <br>
                                    Fecha: <br/>
                                    Cliente: <br/>
                                </td>
                            </tr> --}}
                        </table>
                    </td>
                </tr>
            </table>            
            <hr/>    
         {{--    <div style="float:right; width:250px; height:35px; border: 1px solid #000;  display:flex; flex-direction: row">
                <div style="border-right: 1px solid #000; flex-grow: 1; padding:10px;">Pedido: {{ $order->reference }}</div>                
            </div> --}}
                        
            <h3>Detalle del pedido</h3>
            <table class="table" style="border:1px solid #000; width:100%;" cellpadding="7">
                <thead>
                    <tr>
                        {{-- <th>&nbsp;</th> --}}
                        <th>Artículo</th>
                        <th>Cantidad</th>                        
                        <th>Pedido</th>
                        @if ($order->customer->show_delivery_note_positions)
                        <th>Pos</th>
                        @endif
                    </tr>
                </thead>
                @foreach($order->lines as $line)
                <tr style="border-top:1px solid #999;">
                  {{--   <td style="text-align:left;">
                        <img src="{{ Storage::url($line->article->image_path) }}" height="75"/>
                    </td> --}}
                     <td style="text-align:left; border-top:1px solid 999;">
                        {{ $line->article->reference }} - {{ $line->article->name }}                    
                        </span>
                    </td>
                     <td style="text-align:center; border-top:1px solid 999;">
                        x{{ $line->quantity }}
                    </td>
                    <td style="text-align:center; border-top:1px solid 999;">
                        @if (!empty($line->order_ref))
                            {{ $line->order_ref }}
                        @endif

                        @if (!empty($line->obs))

                            @if (!empty($line->order_ref))
                              <br> <span class="small"> {{ $line->obs }}</small>
                            @endif
                            
                        @endif
                    </td>
                    @if ($order->customer->show_delivery_note_positions)
                    <td style="text-align:center;">
                        {{ $line->article->position }}
                    </td>
                    @endif
                </tr>
                @endforeach
            </table>
        
            <div style="width:100%; height:75px; padding:12px; border:1px solid #999; margin-top:10px;">
                <span style="font-weight:bold; display:inline;">Observaciones: </span>
                <span class="small">{{ nl2br($order->observations) ?? '' }}</span>
            </div>
             
        </div>
        <script>
            window.onload = function()
            {
                window.print();
            }
        </script>
	</body>
</html>